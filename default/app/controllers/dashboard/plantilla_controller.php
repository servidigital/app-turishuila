<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category
 * @package     Controllers
 */
Load::models('cliente','vehiculo','planes');  // carga modelos

class PlantillaController extends BackendController {

    public $page_title = 'Principal';
    public $page_module = 'Plantilla';

    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }

    public function listar() { 
        $this->page_title = 'Listado';
    }     



}
