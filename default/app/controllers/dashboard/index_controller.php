<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category    
 * @package     Controllers 
 */
Load::models('cliente','cotizacion');  // carga modelos

class IndexController extends BackendController {
    
    public $page_title = 'Principal';    
    public $page_module = 'Dashboard';
    
    public function index() {
        
         $usuario_id = Session::get('id');
         $this->count_clientes = Load::model('cliente')->count();
        
         if((Session::get('perfil_id') <= Perfil::ADMINISTRADOR)){
            $this->count_cotizaciones_activas = Load::model('cotizacion')->count("estado=1");
            $this->count_cotizaciones = Load::model('cotizacion')->count();
         }else{
            $this->count_cotizaciones_activas = Load::model('cotizacion')->count("usuario_id=$usuario_id AND estado=1");           
            $this->count_cotizaciones = Load::model('cotizacion')->count("usuario_id=$usuario_id");
         }
         $this->count_solicitudes = Load::model('solicitud')->count("estado=1");
         $this->count_visas = Load::model('visa')->count("estado=1");

         $this->proveedoresAncla = Load::model('proveedor')->find("conditions: estado=1 AND anclar_inicio=1");        
         $this->cotizacion_historial = Load::model('cotizacion_historial')->find("order: id desc");        

         
        $this->page_title = 'Mi escritorio';
        
    }
   

}
