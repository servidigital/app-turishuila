<?php
/**
 *
 * Descripcion: Controlador para el panel principal de las solicitudes
 *
 * @category
 * @package     Controllers
 */
Load::models('solicitud','cotizacion','cliente','cotizacion_item','sistema/usuario','politica','cotizacion_politica','codigo_iata');  // carga modelos
Load::lib('simple_html_dom'); // cargo lib


class SolicitudesController extends BackendController {

    protected function  after_filter() {
        if (Input::isAjax()){
            View::template(null); //si es ajax solo mostramos la vista
        }
    }


    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Solicitudes';
    }


    /**
     * Método principal
     */
    public function index($order='order.id.desc', $page='page.1') {
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;

        $this->page_title = 'Listado de Solicitudes';
        $solicitud = new Solicitud();
        $this->solicitudes = $solicitud->GetListadosolicitud($order, $page);
        $this->order = $order;

        $this->solicitudes = load::model('solicitud')->find("order: id desc");
            if(Input::hasPost('solicitud')) {
                $solicitud = New Solicitud(Input::post('solicitud'));
                if($solicitud->save()){
                    Flash::valid('Solicitud Agregado Correctamente.');
                    return Redirect::toAction("index");

                }else{
                    Flash::error('Error.');

                }

            }

        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $solicitud = new solicitud();
        $this->solicitudes = $solicitud->GetListadosolicitud($order, $page);
        $this->asesores_comerciales = Load::model('sistema/usuario')->find("conditions: perfil_id>=2");


    }

    /**
     * Método para listar
     */
    public function listar($order='order.id.desc', $page='page.1') {
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $solicitud = new cotizacion();
        $this->solicitudes = $solicitud->GetListadoCotizaciones($order, $page);
        $this->asesores_comerciales = Load::model('sistema/usuario')->find("conditions: perfil_id>=2");


        if(Input::hasPost('cotizacion')) {
//            ActiveRecord::beginTrans();
            $cotizacion = new Cotizacion(Input::post('cotizacion'));
            $cotizacion_id = $_POST['cotizacionid'];
            $cotizacion->find_first($cotizacion_id);
            if($cotizacion->update(Input::post('cotizacion'))){
//                ActiveRecord::commitTrans();
                Flash::info('Solicitud sedida correctamente.');
                return Redirect::toAction("listar/$order/$page");
            } else {
//                ActiveRecord::rollbackTrans();
            }
        }


        $this->order = $order;
        $this->page_title = 'Listado de Solicitudes';
    }

    /**
     * Método para buscar clientes
     */
    public function buscar_cliente(){
        // Boton nuevo cliente si no hay resultados
            $this->newcliente = NULL;
        // inicio proceso submit form
         if(Input::hasPost("cl_nombre")) {
            $ref = Input::post("cl_nombre");
            $cliente = New Cliente();
            $busqueda = $cliente->searchCliente("$ref");
            $this->busqueda = $busqueda;
//            $this->busqueda = $cliente->getAjaxCliente($field='nombre', $value="$ref", $order='order.id.asc', $page=1);
            if($busqueda == NULL) {
                Flash::info('No se han encontrado registros');
                // Activo Boton
                $this->newcliente = 1;
            }
         }
    }

    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;

        $solicitud = new cotizacion();
        $solicitudes = $solicitud->getAjaxSolicitudesClientes($field, $value, $order, $page);

        $this->order = $order;
                if(empty($solicitudes->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->solicitudes = $solicitudes;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Clientes';
    }


    /**
     * Método para agregar
     */
    public function agregar($key,$key2=NULL) {
        if(!$cliente_id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $this->cliente = Load::model('cliente')->find_first("$cliente_id");
        $usuario_id =  Session::get('id');

        $this->key = $key;
        $this->key2 = $key2;
        if($key2 == NULL){
            if(Input::hasPost('cotizacion_item')) {


                ActiveRecord::beginTrans();
                $cotizacion = new Cotizacion();
                $cotizacion->cliente_id = $cliente_id;
                $cotizacion->estado = 1;
                $cotizacion->usuario_id = $usuario_id;
//                $cotizacion->token = sha1($cliente_id.$key + date('his'));
                if($cotizacion->save()) {
                    $key_token = Security::setKey($cotizacion->id, 'shw_cliente');
                    $cotizacion->token = $key_token;
                    $cotizacion->update();

                    $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                    $cotizacion_item->cotizacion_id = $cotizacion->id;
                    $cotizacion_item->estado = 1;
                    if($cotizacion_item->save()){
                        ActiveRecord::commitTrans();
                        $key_cotizacion = Security::setKey($cotizacion->id, 'shw_solicitud');
                        Flash::valid('El Item fue Agregado Correctamente.');
                        return Redirect::toAction("agregar/$key/$key_cotizacion");
                    }else{
                        Flash::error("Error Al Guardar Item Solicitud.");
                        return Redirect::toAction("agregar/$key");
                    }
                }else{
                    Flash::error("Error Al Guardar Solicitud");
                    return Redirect::toAction("agregar/$key");
                }
            } // fin haspost cotizacion_item key2 null
                $this->solicitud = NULL;
                $this->items_cotizacion = NULL;

            }else{
                if(!$solicitud_id = Security::getKey($key2, 'shw_solicitud', 'int')) {
                return Redirect::toAction('listar');
                }

                $solicitud = Load::model('cotizacion')->find_first("$solicitud_id");
                $this->solicitud = $solicitud;
                $this->items_cotizacion = Load::model('cotizacion_item')->find("cotizacion_id = $solicitud->id AND estado= 1");

                $cotizacion_item = load::model('cotizacion_item')->find_first("conditions: cotizacion_id = $solicitud->id AND estado= 1","columns: cotizacion_item.origen, cotizacion_item.destino, cotizacion_item.fechain, cotizacion_item.fecha_out, cotizacion_item.adultos, cotizacion_item.infantes, cotizacion_item.habitaciones");
                $this->cotizacion_item = $cotizacion_item;


            if(Input::hasPost('cotizacion_item')) {

                ActiveRecord::beginTrans();
                $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                $cotizacion_item->cotizacion_id = $solicitud->id;
                $cotizacion_item->estado = 1;
                if($cotizacion_item->save()){
                    ActiveRecord::commitTrans();
                    $key_cotizacion = Security::setKey($solicitud->id, 'shw_solicitud');
                    Flash::valid('El Item fue Agregado Correctamente.');
                    return Redirect::toAction("agregar/$key/$key_cotizacion");
                }else{
                    Flash::error("Error Al Guardar Item Solicitud.");
                    return Redirect::toAction("agregar/$key");
                }
            }// fin haspost cotizacion_item key2 lleno ok

            } // fin else key2

            // EXTRAIGO TRM DOLAR
//            traigo variable de session guardada en index
            if(empty($_SESSION['trmHoy'])){
                $this->dolar = 0;
            }else{
                $this->dolar = $_SESSION['trmHoy'];
            }

//            $html = new simple_html_dom();
//            $html->load_file('https://dolar.wilkinsonpc.com.co/');
////            $titles = $html->find('span[class=indicador_numero promedio]');
//            $valordolar = $html->find('span[class=sube-numero]');
//            if(!empty($valordolars)){
//                $titles = $html->find('span[class=baja-numero]');
//            }  else {
//                $titles = $html->find('span[class=sube-numero]');
//            }
//
//            foreach ($titles as $title){
//                $dolar = $title->innertext;
//                $dolar = str_replace (array('$',','),'', $dolar );
//                $this->dolar = $dolar;
//            }



            // Tratamiento de imagenes
      $ruta = RUTA_RAIZ."/app-turishuila/default/public/files/clientes/";
        $this->ruta = $ruta;
        $this->iatas = Load::model('codigo_iata')->getIatasToJson();
        $this->page_title = 'Agregar Solicitud';
    }

   /**
     * Método para editar
     */
	public function editar($key,$key_item=NULL) {
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }

            if(!$key_item == NULL){
                $producto = Load::model('cotizacion_item')->find_first($key_item);
                $this->producto_id = $producto->producto_id;
                $cotizacion_item = New CotizacionItem();
                $this->cotizacion_item = $cotizacion_item->find_first("$key_item");
            }  else {
                $this->producto_id = 0;
            }

            $this->key_item = $key_item ;
            $coti = New Cotizacion();
            $this->solicitud = $coti->find_first($id);
            $this->key2 = Security::setKey($this->solicitud->id, 'shw_cliente');

            $cliente = New cliente();
            $this->cliente = $cliente->find_first($this->solicitud->cliente_id);
            $this->key = Security::setKey($this->cliente->id, 'shw_cliente');

            $cotizacion_items = New CotizacionItem();
            $this->items_cotizacion = $cotizacion_items->find("cotizacion_id=$id AND estado=1");


            if(Input::hasPost('cotizacion_item')) {
                ActiveRecord::beginTrans();
                    $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                    $cotizacion_item->find_first("id=$key_item");
//                    $cotizacion_item->cotizacion_id = $cotizacion->id;
//                    $cotizacion_item->estado = 1;
                    if($cotizacion_item->update(Input::post('cotizacion_item'))){
                        ActiveRecord::commitTrans();
//                        $key_cotizacion = Security::setKey($cotizacion->id, 'shw_solicitud');
                        Flash::valid('El Item fue Editado Correctamente.');
                        return Redirect::toAction("editar/$key/");
                    }else{
                        Flash::error("Error Al Editar Item Solicitud.");
                        return Redirect::toAction("editar/$key");
                    }
                }

            // EXTRAIGO TRM DOLAR
//            traigo variable de session guardada en index
            $this->dolar = $_SESSION['trmHoy'];

//            $html = new simple_html_dom();
//            $html->load_file('https://dolar.wilkinsonpc.com.co/');
////            $titles = $html->find('span[class=indicador_numero promedio]');
//            $valordolar = $html->find('span[class=sube-numero]');
//            if(!empty($valordolars)){
//                $titles = $html->find('span[class=baja-numero]');
//            }  else {
//                $titles = $html->find('span[class=sube-numero]');
//            }
//
//            foreach ($titles as $title){
//                $dolar = $title->innertext;
//                $dolar = str_replace (array('$',','),'', $dolar );
//                $this->dolar = $dolar;
//            }

            $this->page_title = 'Editar Solicitud';
	}

   /**
     * Método para ver
     */
	public function ver($key) {
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }
            $this->key_shw = $key;
            $cotizacion = New Cotizacion();
            $this->cotizacion = $cotizacion->find_first($id);

            $cotizacion_items = New CotizacionItem();
            $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$id AND estado=1");

            $this->page_format = 'html';
            $this->page_title = 'Reporte Html';
	}


   /**
     * Método para Eliminar
     */
	public function eliminar($key,$key2,$keydel) {
	        if(!$id = Security::getKey($keydel, 'del_cliente', 'int')) {
	            return Redirect::toAction('listar');
	        }

                $cotizacion_item = new CotizacionItem();
	        if($cotizacion_item->find_first($id)) {
                    $cotizacion_item->estado = "0";
                    $cotizacion_item->update();
                    Flash::valid("Item Eliminado Exitosamente");
	            return Redirect::toAction("agregar/$key/$key2");
	        }  else {
                    Flash::error('Lo sentimos, no se ha podido establecer la información del Item');
	            return Redirect::toAction("agregar/$key/$key2");
                }
	}


   /**
     * Método para Eliminar Politica
     */
	public function eliminar_politica($key,$key2,$key_politica) {
	        if(!$id = Security::getKey($key_politica, 'del_cliente', 'int')) {
	            return Redirect::toAction('listar');
	        }
                $politica = new CotizacionPolitica();
	        if($politica->find_first($id)) {
                    $politica->delete();
                    Flash::valid("Politica Eliminado Exitosamente");
                    return Redirect::toAction("agregar/$key/$key2");

	        }  else {
                    Flash::error('Lo sentimos, no se ha podido establecer la información del Item');
	            return Redirect::toAction("agregar/$key/$key2");
                }
	}


   /**
     * Método para Enviar por email
     */
	public function send($key) {
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $this->key = $key;

            $solicitud = new Cotizacion();
            if(!$solicitud->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información del Cliente');
                return Redirect::toAction('listar');
            }

            $this->asesor_nombre = Session::get('nombre');
            $this->asesor_apellido = Session::get('apellido');
            $this->asesor_email = Session::get('email');
            $this->solicitud = $solicitud;

            if(Input::hasPost('send')) {
                $send = $_POST['send'];

                $mensaje = $_POST['mensaje'];
                $email = $send['email'];
                $cc = $send['cc'];
                $asunto = $send['asunto'];

                $nombre_asesor =  Session::get('nombre')." ". Session::get('apellido') ;
                $email_asesor =  Session::get('email') ;


                Load::lib('Mailin'); // cargo lib

                $mailin = new Mailin('sistemas@turishuila.com', 'bD9HN4SUQ8Vx31qw');
                $mailin->addTo("$email");
                $mailin->setFrom('sistemas@turishuila.com', 'TURISHUILA');
                $mailin->setReplyTo("$email_asesor","$nombre_asesor");
                if(!empty($cc)){
                    $mailin->setCc($cc);
                }
                $mailin->setSubject("$asunto");
//                $mailin->setText('Hola');
                $mailin->setText("$mensaje");
//                $mailin->setHtml("$mensaje");
                $res = $mailin->send();
                //var_dump($res);

                $obj = json_decode($res);

                if($obj->result == true){
                    Flash::valid("E-MAIL enviado Exitosamente");
                    return Redirect::toAction('listar');

                }  else {
                    Flash::error("Oops! no hemos podido enviar el correo. Por favor intenta mas tarde.");
                }


//    Load::lib('PHPMailer/PHPMailerAutoload'); // cargo lib
//
//    //create an instance of PHPMailer
//    $mail = new PHPMailer();
//
//    //set a host
//    $mail->Host = "smtp.gmail.com";
//
//    //enable SMTP
//    $mail->isSMTP();
//    $mail->SMTPDebug = 1;
//
//    //set authentication to true
//    $mail->SMTPAuth = true;
//
//    //set login details for Gmail account
//    $mail->Username = "sistemas@turishuila.com";
//    $mail->Password = "huila123";
//
//    //set type of protection
//    $mail->SMTPSecure = "ssl"; //or we can use TLS
//
//    //set a port
//    $mail->Port = 465; //or 587 if TLS
//
//    //set subject
//    $mail->Subject = "test email 2";
//
//    //set HTML to true
//    $mail->isHTML(true);
//
//    //set body
//    $mail->Body = "this is our body...<br /><br /><a href='http://google.com'>Google</a> ";
//
//    //add attachment
//
//    //set who is sending an email
//    $mail->setFrom('admin@servidigital.co', 'SB');
//
//    //set where we are sending email (recipients)
//    $mail->addAddress('admin@servidigital.co');
//
//    //send an email
//    if ($mail->send())
//        echo "mail is sent";
//    else
//        echo $mail->ErrorInfo;
}

            $this->page_title = 'Enviar Solicitud';
	}


    /**
     * Método para generar PDF
     */
    public function pdf($id) {

        View::template(NULL);
        $cotizacion = new Cotizacion();
        $solicitud = $cotizacion->find_first($id);
        $this->cotizacion = $solicitud;

        $cotizacion_item = new CotizacionItem();
        $items = $cotizacion_item->find("cotizacion_id=$id");
        $this->cotizacion_items = $items;

    }



    /**
     * Método para agregar Politica
     */
    public function agregar_politica($key,$key2){
        if(Input::hasPost('cotizacion_politica')) {
            ActiveRecord::beginTrans();
            $cotizacion_politica = new CotizacionPolitica(Input::post('cotizacion_politica'));
            if($cotizacion_politica->save()) {
                ActiveRecord::commitTrans();
                Flash::valid("Politica agregada correctamente.");
                return Redirect::to("dashboard/solicitudes/agregar/$key/$key2/");
            } else {
                ActiveRecord::rollbackTrans();
            }
        }

    }

    /**
     * Método para Actualizar Estado
     */
    public function actualizar_estado($key,$estado){
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }
        $cotizacion = New Cotizacion();
        $cotizacion->find_first("id=$id");
        $cotizacion->estado = $estado;
        if($cotizacion->update()){
                Flash::valid("Solicitud Actualizada Correctamente.");
                 Redirect::toAction("listar");
        }
                 Redirect::toAction("listar");

    }

}

