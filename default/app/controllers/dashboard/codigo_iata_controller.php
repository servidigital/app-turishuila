<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los IATA 
 *
 * @category    
 * @package     Controllers 
 */
Load::models('codigo_iata');  // carga modelos

class CodigoIataController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de IATA';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */    
    public function listar($order='order.codigo.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $iata = new CodigoIata();
        $this->iatas = $iata->GetListadoiata($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado de Codigos IATA';
    } 

    /**
     * Método para agregar
     */
    public function agregar() {
        if(Input::hasPost('codigo_iata')) {
            ActiveRecord::beginTrans();
            $iata = new CodigoIata(Input::post('codigo_iata'));
            $iata->estado = 1;
            if($iata->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('El Codigo IATA se ha creado correctamente.');
                return Redirect::toAction("listar/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar IATA'; 
    }


    /**
     * Método para editar
     */    
    public function editar($key) {        
        if(!$id = Security::getKey($key, 'upd_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $iata = new CodigoIata();
        if(!$iata->find_first("conditions: id=$id")) {
            Flash::error('Lo sentimos, no se ha podido establecer la información de la IATA');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('codigo_iata')) {            
            ActiveRecord::beginTrans();            
            $iata = new CodigoIata(Input::post('codigo_iata'));
            $iata->find_first("$id");
            if($iata->update(Input::post('codigo_iata'))){
                ActiveRecord::commitTrans();
                Flash::valid('La IATA se ha actualizado correctamente.');
                return Redirect::toAction('listar');
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->codigo_iata = $iata;
        $this->page_title = 'Actualizar IATA';
        
        
    }  


        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.codigo.asc', $page=1) {        
        $this->page_title = 'Búsqueda de Instructores del sistema';        
    }
           
        
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $iata = new CodigoIata();            
        $iata->find_first("$id");
        if($iata->estado ==1){
            $iata->estado = "0";
            $estado= "IATA Inactivo";            
        }  else {
            $iata->estado = "1";     
            $estado= "IATA Activo";            
        }
        if ($iata->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }
    
    /**
     * Método para subir imágenes
     */
    public function upload() {     
        $upload = new DwUpload('img', 'img/upload/instructores/');
        $upload->setAllowedTypes('png|jpg|gif|jpeg');
        $upload->setEncryptName(TRUE);
        $upload->setSize('3MB', 400, 400, TRUE);
        if(!$data = $upload->save()) { //retorna un array('path'=>'ruta', 'name'=>'nombre.ext');
            $data = array('error'=>TRUE, 'message'=>$upload->getError());
        }
        sleep(1);//Por la velocidad del script no permite que se actualize el archivo
        $this->data = $data;
        View::select(NULL, 'json');
    }     

}
