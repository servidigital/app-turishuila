<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los Proveedor
 *
 * @category    
 * @package     Controllers 
 */
Load::models('tarjeta_asistencia');  // carga modelos

class TarjetaAsistenciaController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Tarjetas de Asistencia';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */
    public function listar($order='order.id.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $tarjetas = new TarjetaAsistencia();
        $this->tarjetas = $tarjetas->GetListadoTarjetas($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado';
    } 
    
    /**
     * Método para agregar
     */
    public function agregar() {
        if(Input::hasPost('tarjeta')) {
            ActiveRecord::beginTrans();
            $tarjeta = new TarjetaAsistencia(Input::post('tarjeta'));
            $tarjeta->estado = 1;
            if($tarjeta->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('La Tarjeta se ha creado correctamente.');
                return Redirect::toAction("listar/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar Tarjeta'; 
    } 
    
   /**
     * Método para ver
     */
	public function ver($key) {        
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $proveedor = new Proveedor();
            if(!$proveedor->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información del Proveedor');    
                return Redirect::toAction('listar');
            }                	                
            $this->proveedor = $proveedor;
            $this->page_title = 'Ver Proveedor';                 	    
	}
        
       
    /**
     * Método para editar
     */    
    public function editar($key) {        
        if(!$id = Security::getKey($key, 'upd_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $tarjeta = new TarjetaAsistencia();
        if(!$tarjeta->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información de la Tarjeta');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('tarjeta')) {            
            ActiveRecord::beginTrans();            
            $tarjeta = new TarjetaAsistencia(Input::post('tarjeta'));
            $tarjeta->find_first($id);
            if($tarjeta->update(Input::post('tarjeta'))){
                ActiveRecord::commitTrans();
                Flash::valid('La Tarjeta se ha actualizado correctamente.');
                return Redirect::toAction('listar');
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->tarjeta = $tarjeta;
        $this->page_title = 'Actualizar Tarjeta';
        
        
    }        
        
        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {        
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;
        
        $cliente = new Cliente();
        $cliente = $cliente->getAjaxClientes($field, $value, $order, $page);        
        if(empty($cliente->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->clientes = $cliente;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Proveedor';        
    }
    
    
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $tarjeta = new TarjetaAsistencia();            
        $tarjeta->find_first("$id");
        if($tarjeta->estado ==1){
            $tarjeta->estado = "0";
            $estado= "Tarjeta Inactivo";            
        }  else {
            $tarjeta->estado = "1";     
            $estado= "Tarjeta Activo";            
        }
        if ($tarjeta->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }
    
    /**
     * Método para subir imágenes
     */
    public function upload() {     
        $upload = new DwUpload('img', 'img/upload/tarjetas_asistencia/');
        $upload->setAllowedTypes('png|jpg|gif|jpeg');
        $upload->setEncryptName(TRUE);
        $upload->setSize('3MB', 400, 400, TRUE);
        if(!$data = $upload->save()) { //retorna un array('path'=>'ruta', 'name'=>'nombre.ext');
            $data = array('error'=>TRUE, 'message'=>$upload->getError());
        }
        sleep(1);//Por la velocidad del script no permite que se actualize el archivo
        $this->data = $data;
        View::select(NULL, 'json');
    }     

}
