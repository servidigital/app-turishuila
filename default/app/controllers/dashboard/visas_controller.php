<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category    
 * @package     Controllers 
 */
Load::models('cliente','cotizacion');  // carga modelos

class VisasController extends BackendController {
    
    public $page_title = 'Principal';    
    public $page_module = 'Visas';
    
    public function index() {         
        $this->page_title = 'Listado de Visas';
        
    }
   

}
