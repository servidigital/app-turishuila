<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los productos
 *
 * @category
 * @package     Controllers
 */
Load::models('vehiculo');  // carga modelos

class VehiculosController extends BackendController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Vehiculos';
    }

    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }

    /**
     * Método para listar
     */
    public function listar() {
        $this->page_title = 'Listado de Vehiculos';
    }



}
