<?php
/**
 *
 * Descripcion: Controlador para el panel principal de las Cotizaciones
 *
 * @category
 * @package     Controllers
 */
Load::models('cotizacion','cliente','cotizacion_item','cotizacion_item_precio','cotizacion_item_costeo','sistema/usuario','politica','cotizacion_politica','codigo_iata');  // carga modelos
Load::lib('simple_html_dom'); // cargo lib
Load::lib('bitly'); // cargo lib


class CotizacionesController extends BackendController {

    protected function  after_filter() {
        if (Input::isAjax()){
            View::template(null); //si es ajax solo mostramos la vista
        }
    }


    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Cotizaciones';
    }


    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }

    public function listarnew() {
        // Redirect::toAction('listar/');
        $this->page_title = 'Listado de Cotizaciones';
    }    

    public function editor(){
        
    }

    /**
     * Método para listar
     */
    public function listar($order='order.id.desc', $page='page.1') {
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $cotizacion = new cotizacion();

        // Session::get('perfil_id') ==Perfil::FIRMA;
        $perfil_comercial = Perfil::COMERCIAL;
        $perfil_administrador = Perfil::ADMINISTRADOR;
        $this->usuarios_comercial = load::model("sistema/usuario")->find("conditions: perfil_id= $perfil_comercial OR perfil_id=$perfil_administrador","columns: id,nombre,perfil_id");

        $this->cotizaciones = $cotizacion->GetListadoCotizaciones($order, $page);
        $this->asesores_comerciales = Load::model('sistema/usuario')->find("conditions: perfil_id>=2");




        if(Input::hasPost('cotizacion')) {
//            ActiveRecord::beginTrans();
            $cotizacion = new Cotizacion(Input::post('cotizacion'));
            $cotizacion_id = $_POST['cotizacionid'];
            $cotizacion->find_first($cotizacion_id);
            if($cotizacion->update(Input::post('cotizacion'))){
//                ActiveRecord::commitTrans();
                Flash::info('cotizacion sedida correctamente.');
                return Redirect::toAction("listar/$order/$page");
            } else {
//                ActiveRecord::rollbackTrans();
            }
        }



        // GUARDO PRECIOS COTIZACION
        if(Input::hasPost('cotizacion_costeo_detalle') || Input::hasPost('cotizacion_costeo_valor')) {
            // Flash::info($cotizacion_item->cotizacion_id);
            // print_r(Input::Post('cotizacion_costeo_detalle'));

            $cotizacionIdCosteo = Input::post('cotizacion_costeo_cotizacion_id');
            // Flash::info("ID $cotizacionIdCosteo");

            // BORRO PRECIOS COTIZACION
            $cotizacion_item_costeo_delete = load::model('cotizacion_item_costeo')->find("conditions: cotizacion_id =$cotizacionIdCosteo");
            foreach ($cotizacion_item_costeo_delete as $cotizacion_delete) {
                // Flash::valid("$cotizacion_delete->id - $cotizacion_delete->cotizacion_id");
                $cotizacion_delete->delete("cotizacion_id=$cotizacionIdCosteo");
            }

            $cotizacion_item_precio_detalle = Input::post('cotizacion_costeo_detalle');
            $cotizacion_item_precio_valor = Input::post('cotizacion_costeo_valor');

            $resultado = array_combine($cotizacion_item_precio_detalle, $cotizacion_item_precio_valor);
            // //  print_r($resultado);

            foreach($resultado as $detalle => $valor){
                $cotizacion_item_detalle = New CotizacionItemCosteo();
                $cotizacion_item_detalle->cotizacion_id = $cotizacionIdCosteo;
                $cotizacion_item_detalle->detalle = $detalle;
                $cotizacion_item_detalle->valor = $valor;
                $cotizacion_item_detalle->save();
            }

            return Redirect::toAction("listar");
        }

        // ACORTADOR DE URL
        // include_once('bitly.php');
        // $params = array();
        // $params['access_token'] = 'a6bbdaa7dc961c56962810ea00c9b33e6bb90ab1';
        // $params['longUrl'] = 'http://138.197.126.96/reporte/cotizacion/ver/1014.08afeada3727';
        // $params['domain'] = 'j.mp';
        // $results = bitly_get('shorten', $params);
        // $results_data = $results['data'];
        // $results_data['url'];




        $this->order = $order;
        $this->page = $page;
        $this->page_title = 'Listado de Cotizaciones';
    }

    /**
     * Método para buscar clientes
     */
    public function buscar_cliente(){
        // Boton nuevo cliente si no hay resultados
            $this->newcliente = NULL;
        // inicio proceso submit form
         if(Input::hasPost("cl_nombre")) {
            $ref = Input::post("cl_nombre");
            $cliente = New Cliente();
            $busqueda = $cliente->searchCliente("$ref");
            // Flash::warning("$ref  fafad");
            $this->busqueda = $busqueda;
//            $this->busqueda = $cliente->getAjaxCliente($field='nombre', $value="$ref", $order='order.id.asc', $page=1);
            if($busqueda == NULL) {
                Flash::info('No se han encontrado registros');
                // Activo Boton
                $this->newcliente = 1;
            }
         }
         $this->page_title = 'Buscar Cliente';

    }

    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=0) {
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;

        Flash::warning($field." ".$value);

        $cotizacion = new cotizacion();
        $Cotizaciones = $cotizacion->getAjaxCotizacionesClientes($field, $value, $order, $page);

        $this->order = $order;
                if(empty($Cotizaciones->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->Cotizaciones = $Cotizaciones;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Clientes';
    }


    /**
     * Método para agregar
     */
    public function agregar($key,$key2=NULL) {
        if(!$cliente_id = Security::getKey($key, 'shw_cliente', 'int')) {
            $cliente_id = $key;
            $key = Security::setKey("$key", 'shw_cliente');
            return Redirect::toAction("listar");
        }


        $this->cliente = Load::model('cliente')->find_first("$cliente_id");
        $usuario_id =  Session::get('id');

        $this->key = $key;
        $this->key2 = $key2;
        if($key2 == NULL){
            if(Input::hasPost('cotizacion_item')) {



                ActiveRecord::beginTrans();
                $cotizacion = new Cotizacion();
                $cotizacion->cliente_id = $cliente_id;
                $cotizacion->estado = 1;
                $cotizacion->usuario_id = $usuario_id;
//                $cotizacion->token = sha1($cliente_id.$key + date('his'));
                if($cotizacion->save()) {
                    // GUARDO PRECIOS COTIZACION
                    if(Input::hasPost('cotizacion_precio_detalle') || Input::hasPost('cotizacion_precio_valor')) {
                        $cotizacion_item_precio_detalle = Input::post('cotizacion_precio_detalle');
                        $cotizacion_item_precio_valor = Input::post('cotizacion_precio_valor');

                        $resultado = array_combine($cotizacion_item_precio_detalle, $cotizacion_item_precio_valor);
                        //  print_r($resultado);

                        foreach($resultado as $detalle => $valor){
                             $cotizacion_item_detalle = New CotizacionItemPrecio();
                             $cotizacion_item_detalle->cotizacion_id = $cotizacion->id;
                             $cotizacion_item_detalle->detalle = $detalle;
                             $cotizacion_item_detalle->valor = $valor;
                             $cotizacion_item_detalle->save();
                        }

                    }



                    $key_token = Security::setKey($cotizacion->id, 'shw_cliente');
                    $cotizacion->token = $key_token;

                    // ACORTADOR DE URL
                    $dominio  = DOMINIO;
                    $params = array();
                    $params['access_token'] = 'a6bbdaa7dc961c56962810ea00c9b33e6bb90ab1';
                    $params['longUrl'] = $dominio.'reporte/cotizacion/ver/'.$cotizacion->token;
                    $params['domain'] = 'j.mp';
                    $results = bitly_get('shorten', $params);
                    $results_data = $results['data'];
                    $cotizacion->url_corta = $results_data['url'];
                    $cotizacion->update();

                    $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                    $cotizacion_item->cotizacion_id = $cotizacion->id;
                    $cotizacion_item->estado = 1;
                    if($cotizacion_item->save()){
                        ActiveRecord::commitTrans();
                        $key_cotizacion = Security::setKey($cotizacion->id, 'shw_cotizacion');
                        Flash::valid('El Item fue Agregado Correctamente.');
                        return Redirect::toAction("agregar/$key/$key_cotizacion");
                    }else{
                        Flash::error("Error Al Guardar Item cotizacion.");
                        return Redirect::toAction("agregar/$key");
                    }
                }else{
                    Flash::error("Error Al Guardar cotizacion");
                    return Redirect::toAction("agregar/$key");
                }
            } // fin haspost cotizacion_item key2 null
                $this->cotizacion = NULL;
                $this->items_cotizacion = NULL;

            }else{
                if(!$cotizacion_id = Security::getKey($key2, 'shw_cotizacion', 'int')) {
                return Redirect::toAction('listar');
                }

                $cotizacion = Load::model('cotizacion')->find_first("$cotizacion_id");
                $this->cotizacion = $cotizacion;
                $this->items_cotizacion = Load::model('cotizacion_item')->find("cotizacion_id = $cotizacion->id AND estado= 1");

                $cotizacion_item = load::model('cotizacion_item')->find_first("conditions: cotizacion_id = $cotizacion->id AND estado= 1","columns: cotizacion_item.origen, cotizacion_item.destino, cotizacion_item.fechain, cotizacion_item.fecha_out, cotizacion_item.adultos, cotizacion_item.infantes, cotizacion_item.habitaciones");
                $this->cotizacion_item = $cotizacion_item;


            if(Input::hasPost('cotizacion_item')) {

                ActiveRecord::beginTrans();
                $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                $cotizacion_item->cotizacion_id = $cotizacion->id;
                $cotizacion_item->estado = 1;
                if($cotizacion_item->save()){
                    ActiveRecord::commitTrans();
                    $key_cotizacion = Security::setKey($cotizacion->id, 'shw_cotizacion');
                    Flash::valid('El Item fue Agregado Correctamente.');
                    return Redirect::toAction("agregar/$key/$key_cotizacion");
                }else{
                    Flash::error("Error Al Guardar Item cotizacion.");
                    return Redirect::toAction("agregar/$key");
                }
            }// fin haspost cotizacion_item key2 lleno ok

            } // fin else key2

            // EXTRAIGO TRM DOLAR
//            traigo variable de session guardada en index
            if(empty($_SESSION['trmHoy'])){
                $this->dolar = 0;
            }else{
                $this->dolar = $_SESSION['trmHoy'];
            }

//            $html = new simple_html_dom();
//            $html->load_file('https://dolar.wilkinsonpc.com.co/');
////            $titles = $html->find('span[class=indicador_numero promedio]');
//            $valordolar = $html->find('span[class=sube-numero]');
//            if(!empty($valordolars)){
//                $titles = $html->find('span[class=baja-numero]');
//            }  else {
//                $titles = $html->find('span[class=sube-numero]');
//            }
//
//            foreach ($titles as $title){
//                $dolar = $title->innertext;
//                $dolar = str_replace (array('$',','),'', $dolar );
//                $this->dolar = $dolar;
//            }



        // Tratamiento de imagenes
        $ruta = RUTA_RAIZ."/app-turishuila/default/public/files/clientes/";
        $this->ruta = $ruta;
        $this->iatas = Load::model('codigo_iata')->getIatasToJson();

        // EFECTIVIDAD CLIENTE
        $solicitudes = new Cotizacion();
        $cliente_id = $cliente_id;
        $count_solicitudes = $solicitudes->count("cliente_id=$cliente_id");
        if(empty($count_solicitudes)){ $count_solicitudes =1;};            
        $count_aceptado = $solicitudes->count("cliente_id=$cliente_id AND estado=4");
        if(empty($count_aceptado)){ $count_aceptado =0;};
        $porcetaje_calificacion = ($count_aceptado*100)/$count_solicitudes;
        $this->porcetaje_calificacion = round($porcetaje_calificacion);   
        
        
        $this->page_title = 'Agregar cotizacion';
    }

   /**
     * Método para editar
     */
	public function editar($key,$key_item=NULL) {
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }

            if(!$key_item == NULL){
                $producto = Load::model('cotizacion_item')->find_first($key_item);
                $this->producto_id = $producto->producto_id;
                $cotizacion_item = New CotizacionItem();
                $this->cotizacion_item = $cotizacion_item->find_first("$key_item");
            }  else {
                $this->producto_id = 0;
            }

            $this->key_item = $key_item ;
            $coti = New Cotizacion();
            $this->cotizacion = $coti->find_first($id);
            $this->key2 = Security::setKey($this->cotizacion->id, 'shw_cliente');

            $cliente = New cliente();
            $this->cliente = $cliente->find_first($this->cotizacion->cliente_id);
            $this->key = Security::setKey($this->cliente->id, 'shw_cliente');

            $cotizacion_items = New CotizacionItem();
            $this->items_cotizacion = $cotizacion_items->find("cotizacion_id=$id AND estado=1");


            if(Input::hasPost('cotizacion_item')) {
                ActiveRecord::beginTrans();
                    $cotizacion_item = new CotizacionItem(Input::post('cotizacion_item'));
                    $cotizacion_item->find_first("id=$key_item");
//                    $cotizacion_item->cotizacion_id = $cotizacion->id;
//                    $cotizacion_item->estado = 1;
                    if($cotizacion_item->update(Input::post('cotizacion_item'))){
                        ActiveRecord::commitTrans();

                        // BORRO PRECIOS COTIZACION
                        $cotizacion_item_precio_delete = load::model('cotizacion_item_precio')->find("conditions: cotizacion_id =$cotizacion_item->cotizacion_id");
                        foreach ($cotizacion_item_precio_delete as $cotizacion_delete) {
                            // Flash::valid("$cotizacion_delete->id - $cotizacion_delete->cotizacion_id");
                            $cotizacion_delete->delete("cotizacion_id=$cotizacion_item->cotizacion_id");
                        }

                        // GUARDO PRECIOS COTIZACION
                        if(Input::hasPost('cotizacion_precio_detalle') || Input::hasPost('cotizacion_precio_valor')) {
                            // Flash::info($cotizacion_item->cotizacion_id);

                            $cotizacion_item_precio_detalle = Input::post('cotizacion_precio_detalle');
                            $cotizacion_item_precio_valor = Input::post('cotizacion_precio_valor');

                            $resultado = array_combine($cotizacion_item_precio_detalle, $cotizacion_item_precio_valor);
                            //  print_r($resultado);

                            foreach($resultado as $detalle => $valor){
                                $cotizacion_item_detalle = New CotizacionItemPrecio();
                                $cotizacion_item_detalle->cotizacion_id = $cotizacion_item->cotizacion_id;
                                $cotizacion_item_detalle->detalle = $detalle;
                                $cotizacion_item_detalle->valor = $valor;
                                $cotizacion_item_detalle->save();
                            }

                        }

//                        $key_cotizacion = Security::setKey($cotizacion->id, 'shw_cotizacion');
                        Flash::valid('El Item fue Editado Correctamente.');
                        // return Redirect::toAction("editar/$key/");
                        return Redirect::toAction("listar");
                    }else{
                        Flash::error("Error Al Editar Item cotizacion.");
                        // return Redirect::toAction("editar/$key");
                        return Redirect::toAction("listar");
                    }
                }

            // EXTRAIGO TRM DOLAR
//            traigo variable de session guardada en index
                if(empty($_SESSION['trmHoy'])){
                    $this->dolar = 0;
                }else{
                    $this->dolar = $_SESSION['trmHoy'];
                }

            // EFECTIVIDAD CLIENTE
            $solicitudes = new Cotizacion();
            $cliente_id = $this->cotizacion->cliente_id;
            $count_solicitudes = $solicitudes->count("cliente_id=$cliente_id");
            if(empty($count_solicitudes)){ $count_solicitudes =1;};            
            $count_aceptado = $solicitudes->count("cliente_id=$cliente_id AND estado=4");
            if(empty($count_aceptado)){ $count_aceptado =0;};
            $porcetaje_calificacion = ($count_aceptado*100)/$count_solicitudes;
            $this->porcetaje_calificacion = round($porcetaje_calificacion);                

            $this->page_title = 'Editar cotizacion';
	}

   /**
     * Método para ver
     */
	public function ver($key) {
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }
            $this->key_shw = $key;
            $cotizacion = New Cotizacion();
            $this->cotizacion = $cotizacion->find_first($id);

            $cotizacion_items = New CotizacionItem();
            $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$id AND estado=1");

            $this->page_format = 'html';
            $this->page_title = 'Reporte Html';
	}


   /**
     * Método para Eliminar
     */
	public function eliminar($key,$key2,$keydel) {
	        if(!$id = Security::getKey($keydel, 'del_cliente', 'int')) {
	            return Redirect::toAction('listar');
	        }

                $cotizacion_item = new CotizacionItem();
	        if($cotizacion_item->find_first($id)) {
                    $cotizacion_item->estado = "0";
                    $cotizacion_item->update();
                    Flash::valid("Item Eliminado Exitosamente");
	            return Redirect::toAction("agregar/$key/$key2");
	        }  else {
                    Flash::error('Lo sentimos, no se ha podido establecer la información del Item');
	            return Redirect::toAction("agregar/$key/$key2");
                }
	}


   /**
     * Método para Eliminar Politica
     */
	public function eliminar_politica($key,$key2,$key_politica) {
	        if(!$id = Security::getKey($key_politica, 'del_cliente', 'int')) {
	            return Redirect::toAction('listar');
	        }
                $politica = new CotizacionPolitica();
	        if($politica->find_first($id)) {
                    $politica->delete();
                    Flash::valid("Politica Eliminado Exitosamente");
                    return Redirect::toAction("agregar/$key/$key2");

	        }  else {
                    Flash::error('Lo sentimos, no se ha podido establecer la información del Item');
	            return Redirect::toAction("agregar/$key/$key2");
                }
	}


   /**
     * Método para Enviar por email
     */
	public function send($key) {
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $this->key = $key;

            $cotizacion = new Cotizacion();
            if(!$cotizacion->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información del Cliente');
                return Redirect::toAction('listar');
            }

            $this->asesor_nombre = Session::get('nombre');
            $this->asesor_apellido = Session::get('apellido');
            $this->asesor_email = Session::get('email');
            $this->cotizacion = $cotizacion;

            if(Input::hasPost('send')) {
                $send = $_POST['send'];

                $mensaje = $_POST['mensaje'];
                $email = $send['email'];
                $cc = $send['cc'];
                $asunto = $send['asunto'];

                $nombre_asesor =  Session::get('nombre')." ". Session::get('apellido') ;
                $email_asesor =  Session::get('email') ;


                Load::lib('Mailin'); // cargo lib

                $mailin = new Mailin('sistemas@turishuila.com', 'bD9HN4SUQ8Vx31qw');
                $mailin->addTo("$email");
                $mailin->setFrom('sistemas@turishuila.com', 'TURISHUILA');
                $mailin->setReplyTo("$email_asesor","$nombre_asesor");
                if(!empty($cc)){
                    $mailin->setCc($cc);
                }
                $mailin->setSubject("$asunto");
//                $mailin->setText('Hola');
                $mailin->setText("$mensaje");
//                $mailin->setHtml("$mensaje");
                $res = $mailin->send();
                //var_dump($res);

                $obj = json_decode($res);

                if($obj->result == true){
                    Flash::valid("E-MAIL enviado Exitosamente");
                    return Redirect::toAction('listar');

                }  else {
                    Flash::error("Oops! no hemos podido enviar el correo. Por favor intenta mas tarde.");
                }


//    Load::lib('PHPMailer/PHPMailerAutoload'); // cargo lib
//
//    //create an instance of PHPMailer
//    $mail = new PHPMailer();
//
//    //set a host
//    $mail->Host = "smtp.gmail.com";
//
//    //enable SMTP
//    $mail->isSMTP();
//    $mail->SMTPDebug = 1;
//
//    //set authentication to true
//    $mail->SMTPAuth = true;
//
//    //set login details for Gmail account
//    $mail->Username = "sistemas@turishuila.com";
//    $mail->Password = "huila123";
//
//    //set type of protection
//    $mail->SMTPSecure = "ssl"; //or we can use TLS
//
//    //set a port
//    $mail->Port = 465; //or 587 if TLS
//
//    //set subject
//    $mail->Subject = "test email 2";
//
//    //set HTML to true
//    $mail->isHTML(true);
//
//    //set body
//    $mail->Body = "this is our body...<br /><br /><a href='http://google.com'>Google</a> ";
//
//    //add attachment
//
//    //set who is sending an email
//    $mail->setFrom('admin@servidigital.co', 'SB');
//
//    //set where we are sending email (recipients)
//    $mail->addAddress('admin@servidigital.co');
//
//    //send an email
//    if ($mail->send())
//        echo "mail is sent";
//    else
//        echo $mail->ErrorInfo;
}

            $this->page_title = 'Enviar cotizacion';
	}


    /**
     * Método para generar PDF
     */
    public function pdf($id) {

        View::template(NULL);
        $cotizacion = new Cotizacion();
        $cotizacion = $cotizacion->find_first($id);
        $this->cotizacion = $cotizacion;

        $cotizacion_item = new CotizacionItem();
        $items = $cotizacion_item->find("cotizacion_id=$id");
        $this->cotizacion_items = $items;

    }



    /**
     * Método para agregar Politica
     */
    public function agregar_politica($key,$key2){
        if(Input::hasPost('cotizacion_politica')) {
            ActiveRecord::beginTrans();
            $cotizacion_politica = new CotizacionPolitica(Input::post('cotizacion_politica'));
            if($cotizacion_politica->save()) {
                ActiveRecord::commitTrans();
                Flash::valid("Politica agregada correctamente.");
                return Redirect::to("dashboard/cotizaciones/agregar/$key/$key2/");
            } else {
                ActiveRecord::rollbackTrans();
            }
        }

    }

    /**
     * Método para Actualizar Estado
     */
    public function actualizar_estado($key,$estado){
       if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }
        $cotizacion = New Cotizacion();
        $cotizacion->find_first("id=$id");
        $cotizacion->estado = $estado;
        if($cotizacion->update()){
                Flash::valid("cotizacion Actualizada Correctamente.");
                 Redirect::toAction("listar");
        }
                 Redirect::toAction("listar");

    }


	public function autourl() {
        $cotizacion = New Cotizacion();
        $this->cotizaciones = $cotizacion->find("conditions: url_corta IS NULL ","order: id desc");

        $cotizaciones = $cotizacion->find("conditions: url_corta IS NULL ","order: id desc");
        foreach($cotizaciones as $cotizacion){
            $cotizacion_up = New Cotizacion();
            $cotizacion_up->find_first("id=$cotizacion->id");

            // ACORTADOR DE URL
            $params = array();
            $params['access_token'] = 'a6bbdaa7dc961c56962810ea00c9b33e6bb90ab1';
            $params['longUrl'] = 'http://138.197.126.96/reporte/cotizacion/ver/'.$cotizacion->token;
            $params['domain'] = 'j.mp';
            $results = bitly_get('shorten', $params);
            $results_data = $results['data'];
            $cotizacion_up->url_corta = $results_data['url'];
            $cotizacion_up->update();

        }
        // var_dump($cotizaciones);
        $this->page_format = 'html';
        $this->page_title = 'Auto Url';
     }    

}

