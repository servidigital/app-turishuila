<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los Clientes
 *
 * @category    
 * @package     Controllers 
 */
Load::models('cliente','cotizacion');  // carga modelos

class ClientesController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Clientes';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */
    public function listar($order='order.id.desc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $cliente = new Cliente();
        $this->clientes = $cliente->GetListadoCliente($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado de clientes';
    } 
    
    /**
     * Método para agregar
     */
    public function agregar() {

        if(Input::hasPost('cliente')) {
            ActiveRecord::beginTrans();
            $cliente = new cliente(Input::post('cliente'));
            $cliente->estado = 1;
            $buscarClienteSeleccion = load::model('cliente')->find("conditions: email='$cliente->email' OR celular=$cliente->celular");
            $this->clienteSeleccion = $buscarClienteSeleccion;


            if($cliente->exists("celular=$cliente->celular") || $cliente->exists("email='$cliente->email'") ){
                if($cliente->exists("celular=$cliente->celular")){
                    Flash::error('Existe registro Celular en la base de datos '.$cliente->celular);
                }

                if($cliente->exists("email='$cliente->email'")){
                    Flash::error('Existe registro Email en la base de datos '.$cliente->email);
                }                

            }else{
                if($cliente->save()) {
                    ActiveRecord::commitTrans();
                    Flash::valid('El Cliente se ha creado correctamente.');
                    $key_shw = Security::setKey($usuario->id, 'shw_cliente');
                    return Redirect::toAction("ver/$key_shw");
                } else {
                    ActiveRecord::rollbackTrans();
                }                 
            }

           
        }
        $this->page_title = 'Agregar Cliente'; 
    } 
    
    
    /**
     * Método para Editar
     */
    public function editar($key) {        
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            
        $cliente = new cliente();
        if(!$cliente->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información del Cliente');    
            return Redirect::toAction('listar');
        }                
                    
        if(Input::hasPost('cliente')) {            
            ActiveRecord::beginTrans();            
            $cliente = new cliente(Input::post('cliente'));
            $cliente->find_first($id);
            if($cliente->update(Input::post('cliente'))){
                ActiveRecord::commitTrans();
                Flash::valid('El Cliente se ha actualizado correctamente.');
                $key_shw = Security::setKey($cliente->id, 'shw_cliente');
                return Redirect::to("dashboard/clientes/ver/$key_shw");
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }   
        $this->cliente = $cliente;
        $this->page_title = 'Editar Cliente'; 
    } 
        
    
   /**
     * Método para ver
     */
	public function ver($key) {        
	        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
	            return Redirect::toAction('listar');
	        }
                $solicitudes = new Cotizacion();
                $count_solicitudes = $solicitudes->count("cliente_id=$id");
                if(empty($count_solicitudes)){ $count_solicitudes =1;};
                
                $count_aceptado = $solicitudes->count("cliente_id=$id AND estado=4");
                if(empty($count_aceptado)){ $count_aceptado =0;};
                $porcetaje_calificacion = ($count_aceptado*100)/$count_solicitudes;
                $this->porcetaje_calificacion = round($porcetaje_calificacion);

                
                $cliente = new Cliente();
	        if(!$cliente->find_first($id)) {
	            Flash::error('Lo sentimos, no se ha podido establecer la información del Cliente');    
	            return Redirect::toAction('listar');
	        }                	                
	        $this->cliente = $cliente;
                $this->solicitudes_cl = Load::model("cotizacion")->find("cliente_id =$cliente->id");
            $this->page_title = 'Ver Cliente';  
            
            // Tratamiento de imagenes
//            $ruta = "/home/turishui/app.turishuila.com/default/public/files/clientes/$id";
//            $ruta = "/Users/avilac3/web/app-turishuila/default/public/files/clientes/$id";
              $ruta = RUTA_RAIZ."files/clientes/$id";
            
            $this->ruta = $ruta;
            $this->key = $key;            
            $this->id = $id;            
	}
        
        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {        
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;
        
        $cliente = new Cliente();
        $cliente = $cliente->getAjaxClientes($field, $value, $order, $page);        
        if(empty($cliente->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->clientes = $cliente;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Clientes';        
    }
    
    
    /**
     * Método para Adjuntos
     */
    public function adjunto($key) {
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('listar');
        }        
                
        // Tratamiento de imagenes
//        $ruta = "/var/www/default/public/files/clientes/$id";
        //$ruta = "/Users/avilac3/web/app-turishuila/default/public/files/clientes/$id";
        $ruta = RUTA_RAIZ."files/clientes/$id";
        
        $this->ruta = $ruta;
        $this->key = $key;

    
        if (Input::hasPost('oculto')) {  //para saber si se envió el form            
            if (!file_exists($ruta)) {
                mkdir($ruta, 0777);
            }                        
            $file = Upload::factory('archivo');//llamamos a la libreria y le pasamos el nombre del campo file del formulario
            $file->setPath($ruta);
            $file->setExtensions(array('zip','rar','jpg','png','pdf')); //le asignamos las extensiones a permitir
            if ($file->isUploaded()) { 
                if ($file->save()) {
                    Flash::valid('Archivo subido correctamente...!!!');
                }
            }else{
                    Flash::warning('No se ha Podido Subir el Archivo...!!!');
            }
        }
        

        if (Input::hasPost('filename')){   // Opcion para Eliminar Archivos Adjuntos
            $archivoname=$_POST['filename'];
            Flash::info($archivoname);
                if (file_exists($ruta)) {
                    $dir = $ruta.'/'.$archivoname;
                    if(unlink($dir)){
                        Flash::info("El archivo fue borrado Exitosamente");                
                    }  else {
                        Flash::danger("Este Archivo no existe");
                    }
                }                    
                 Redirect::to("dashboard/clientes/ver/$key");
        }
        Redirect::to("dashboard/clientes/ver/$key");        
        $this->page_title = 'Archivos Adjuntos';
        
    }

    public function adjunto_eliminar($id,$archivo){      
//                $ruta = "/home/turishui/app.turishuila.com/default/public/files/clientes/$id";
                //$ruta = "/Users/avilac3/web/app-turishuila/default/public/files/clientes/$id";
                $ruta = RUTA_RAIZ."files/clientes/$id";
                
                if (file_exists($ruta)) {
                    $dir = $ruta.'/'.$archivo;
                    if(unlink($dir)){
                        Flash::valid("El archivo fue borrado Exitosamente");                
                    }  else {
                        Flash::danger("Este Archivo no existe");
                    }
                }          
                $key_shw = Security::setKey($id, 'shw_cliente');
                 Redirect::to("dashboard/clientes/ver/$key_shw");        
        
    }    

}
