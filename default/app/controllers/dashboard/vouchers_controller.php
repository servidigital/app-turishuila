<?php
/**
 *
 * Descripcion: Controlador para el panel Vouchers
 *
 * @category    
 * @package     Controllers 
 */
Load::models('cliente','cotizacion','voucher');  // carga modelos

class VouchersController extends BackendController {
    
    public $page_title = 'Voucher';
    
    public $page_module = 'Gestión de Vouchers';
    
    public function index($key,$id_voucher = NULL) {
        $id = Security::getKey($key, 'shw_cliente', 'int'); 
        $this->id = $id;
        $this->key = $key;
        $cotizacion = New Cotizacion();        
        if(!$cotizacion = $cotizacion->find_first("conditions: id=$id")) {
            Flash::error('Lo sentimos, no se ha podido establecer la información de la Solicitud');    
            return Redirect::to('dashboard/solicitudes/');
        }
             
                
    }   
    
    /**
     * Método para agregar
     */
    public function agregar() {        
        if(Input::hasPost('voucher')) {
//            ActiveRecord::beginTrans();
            $usuario_id = Session::get('id');
            $voucher = new Voucher(Input::post('voucher'));
            $voucher->usuario_id = $usuario_id;
            if($voucher->save()) {
//                ActiveRecord::commitTrans();
                $key_shw = Security::setKey($voucher->cotizacion_id, 'shw_cliente');
                Flash::valid('El Voucher se ha creado correctamente.');
                Redirect::to("dashboard/vouchers/index/$key_shw");
//                Redirect::toAction("ver/$key_shw");
//                Redirect::to("dashboard/solicitudes/listar/");
            } else {
//                ActiveRecord::rollbackTrans();
                $key_shw = Security::setKey($voucher->cotizacion_id, 'shw_cliente');
                Flash::error('Error.');
                Redirect::to("dashboard/vouchers/index/$key_shw");
                
            }            
        }
    }
    
    
    
    /**
     * Método para editar
     */    
    public function editar($key,$id_voucher) {        
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction("index/$key");
        }
        $this->id = $id;
        $voucher = new Voucher();
        if(!$voucher->find_first($id_voucher)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información del Voucher');    
            return Redirect::toAction("index/$key");
        }                
        
        if(Input::hasPost('voucher')) {            
            ActiveRecord::beginTrans();            
            $voucher = new Voucher(Input::post('voucher'));
            $voucher->find_first($id_voucher);
            if($voucher->update(Input::post('voucher'))){
                ActiveRecord::commitTrans();
                Flash::warning('El Voucher se ha actualizado correctamente.');
                return Redirect::toAction("index/$key");
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->voucher = $voucher;
        $this->page_title = 'Actualizar Voucher';
        
    }    
        
    /**
     * Método para Eliminar
     */
    public function eliminar($key,$id_voucher) { 
       $id = Security::getKey($key, 'shw_cliente', 'int');
        
        $vouchers = new Voucher();
        $vouchers->find_first("conditions: id=$id_voucher AND cotizacion_id=$id");
        if($vouchers->delete()){
            Flash::error('El Voucher se ha Eliminado correctamente.');
            return Redirect::toAction("index/$key/");            
        }  else {
            Flash::error('ERROR.');
            return Redirect::toAction("index/$key/");             
        }
        
    }    

}
