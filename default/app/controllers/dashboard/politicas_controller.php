<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los Proveedor
 *
 * @category    
 * @package     Controllers 
 */
Load::models('politica');  // carga modelos

class PoliticasController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Politicas';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */
    public function listar($order='order.id.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $politica = new Politica();
        $this->politicas = $politica->GetListadopolitica($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado de Politicas';
    } 
    
    /**
     * Método para agregar
     */
    public function agregar() {
        if(Input::hasPost('politica')) {
            ActiveRecord::beginTrans();
            $politica = new Politica(Input::post('politica'));
            $politica->estado = 1;
            if($politica->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('La Politica se ha creado correctamente.');
                return Redirect::toAction("listar/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar Politica'; 
    } 
    
   /**
     * Método para ver
     */
	public function ver($key) {        
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $politica = new Politica();
            if(!$politica->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información de la Politica');    
                return Redirect::toAction('listar');
            }                	                
            $this->politica = $politica;
            $this->page_title = 'Ver Politica';                 	    
	}
        
       
    /**
     * Método para editar
     */    
    public function editar($key) {        
        if(!$id = Security::getKey($key, 'upd_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $politica = new Politica();
        if(!$politica->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información de la Politica');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('politica')) {            
            ActiveRecord::beginTrans();            
            $politica = new Politica(Input::post('politica'));
            $politica->find_first($id);
            if($politica->update(Input::post('politica'))){
                ActiveRecord::commitTrans();
                Flash::valid('La Politica se ha actualizado correctamente.');
                return Redirect::toAction('listar');
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->politica = $politica;
        $this->page_title = 'Actualizar Politica';
        
        
    }        
        
        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {        
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;
        
        $politica = new Politica();
        $politica = $politica->getAjaxPoliticas($field, $value, $order, $page);        
        if(empty($politica->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->clientes = $cliente;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Proveedor';        
    }
    
    
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $politica = new Politica();            
        $politica->find_first("$id");
        if($politica->estado ==1){
            $politica->estado = "0";
            $estado= "Politica Inactivo";            
        }  else {
            $politica->estado = "1";     
            $estado= "Politica Activo";            
        }
        if ($politica->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }
    
}
