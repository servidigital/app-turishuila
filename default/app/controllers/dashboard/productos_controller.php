<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los productos
 *
 * @category    
 * @package     Controllers 
 */
Load::models('producto','producto','proveedor_producto');  // carga modelos

class ProductosController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Productos';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */
    public function listar($order='order.id.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $producto = new Producto();
        $this->productos = $producto->GetListadoproducto($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado de Producutos';
    } 
    
    /**
     * Método para agregar
     */
    public function agregar() {
        if(Input::hasPost('producto')) {
            ActiveRecord::beginTrans();
            $producto = new Producto(Input::post('producto'));
            $producto->estado = 1;
            if($producto->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('El Producto se ha creado correctamente.');
                return Redirect::toAction("listar/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar Producto'; 
    } 
    
    
    /**
     * Método para agregar_proveedor
     */
    public function agregar_proveedor() {
        if(Input::hasPost('proveedor_producto')) {
            ActiveRecord::beginTrans();
            $proveedor_producto = new ProveedorProducto(Input::post('proveedor_producto'));
            $key_upd = Security::setKey($proveedor_producto->producto_id, 'upd_cliente');
            if($proveedor_producto->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('El Proveedor fue agregado correctamente.');
                return Redirect::toAction("editar/$key_upd");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
    } 

    
    /**
     * Método para eliminar_proveedor
     */
    public function eliminar_proveedor($key,$proveedor_id) {
        $key_upd = Security::setKey("$key", 'upd_cliente');
        $proveedor_producto = New ProveedorProducto();
        $proveedor_producto->find_first("proveedor_id=$proveedor_id");
        if($proveedor_producto->delete()){
            Flash::valid("Proveedor Eliminado Exitosamente.");
            return Redirect::toAction("editar/$key_upd");            
        }

        
    }     
    
    
   /**
     * Método para ver
     */
	public function ver($key) {        
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $producto = new Producto();
            if(!$producto->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información del Producto');    
                return Redirect::toAction('listar');
            }                	                
            $this->producto = $producto;
            $this->page_title = 'Ver Producto';                 	    
	}
        
       
    /**
     * Método para editar
     */    
    public function editar($key) {        
        if(!$id = Security::getKey($key, 'upd_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $producto = new Producto();
        if(!$producto->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información del Producto');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('producto')) {            
            ActiveRecord::beginTrans();            
            $producto = new Producto(Input::post('producto'));
            $producto->find_first($id);
            if($producto->update(Input::post('producto'))){
                ActiveRecord::commitTrans();
                Flash::valid('El Producto se ha actualizado correctamente.');
                return Redirect::toAction('listar');
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->producto = $producto;
        $this->page_title = 'Actualizar Producto';
        
        
    }        
        
        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {        
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;
        
        $cliente = new Cliente();
        $cliente = $cliente->getAjaxClientes($field, $value, $order, $page);        
        if(empty($cliente->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->clientes = $cliente;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Proveedor';        
    }
    
    
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $producto = new Producto();            
        $producto->find_first("$id");
        if($producto->estado ==1){
            $producto->estado = "0";
            $estado= "Producto $producto->nombre Inactivo";            
        }  else {
            $producto->estado = "1";     
            $estado= "Producto $producto->nombre Activo";            
        }
        if ($producto->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }
  

}
