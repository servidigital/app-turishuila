<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los Proveedor
 *
 * @category    
 * @package     Controllers 
 */
Load::models('proveedor');  // carga modelos

class ProveedoresController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Proveedores';
    }
    
    /**
     * Método principal
     */
    public function index() {
        Redirect::toAction('listar/');
    }
    
    /**
     * Método para listar
     */
    public function listar($order='order.id.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $proveedor = new Proveedor();
        $this->proveedores = $proveedor->GetListadoproveedor($order, $page);
        $this->order = $order;        
        $this->page_title = 'Listado de Proveedores';
    } 
    
    /**
     * Método para agregar
     */
    public function agregar() {
        if(Input::hasPost('proveedor')) {
            ActiveRecord::beginTrans();
            $proveedor = new Proveedor(Input::post('proveedor'));
            $proveedor->estado = 1;
            if($proveedor->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('El Proveedor se ha creado correctamente.');
                return Redirect::toAction("listar/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar Proveedor'; 
    } 
    
   /**
     * Método para ver
     */
	public function ver($key) {        
            if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
                return Redirect::toAction('listar');
            }
            $proveedor = new Proveedor();
            if(!$proveedor->find_first($id)) {
                Flash::error('Lo sentimos, no se ha podido establecer la información del Proveedor');    
                return Redirect::toAction('listar');
            }                	                
            $this->proveedor = $proveedor;
            $this->page_title = 'Ver Proveedor';                 	    
	}
        
       
    /**
     * Método para editar
     */    
    public function editar($key) {        
        if(!$id = Security::getKey($key, 'upd_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $proveedor = new Proveedor();
        if(!$proveedor->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información del Proveedor');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('proveedor')) {            
            ActiveRecord::beginTrans();            
            $proveedor = new Proveedor(Input::post('proveedor'));
            $proveedor->find_first($id);
            if($proveedor->update(Input::post('proveedor'))){
                ActiveRecord::commitTrans();
                Flash::valid('El Proveedor se ha actualizado correctamente.');
                return Redirect::toAction('listar');
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }        
        $this->proveedor = $proveedor;
        $this->page_title = 'Actualizar Proveedor';
        
        
    }        
        
        
    /**
     * Método para buscar
     */
    public function buscar($field='nombre', $value='none', $order='order.id.asc', $page=1) {        
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $field = (Input::hasPost('field')) ? Input::post('field') : $field;
        $value = (Input::hasPost('field')) ? Input::post('value') : $value;
        
        $cliente = new Cliente();
        $cliente = $cliente->getAjaxClientes($field, $value, $order, $page);        
        if(empty($cliente->items)) {
            Flash::info('No se han encontrado registros');
        }
        $this->clientes = $cliente;
        $this->order = $order;
        $this->field = $field;
        $this->value = $value;
        $this->page_title = 'Búsqueda de Proveedor';        
    }
    

    /**
     * Método para Anclar al Inicio
     */    
    public function anclar($key) {        
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('listar');
        }
        $proveedor = new Proveedor();            
        $proveedor->find_first("$id");
        if($proveedor->anclar_inicio ==1){
            $proveedor->anclar_inicio = "0";
            $estado= "No Anclado";            
        }  else {
            $proveedor->anclar_inicio = "1";     
            $estado= "Anclado al Inicio";            
        }
        if ($proveedor->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }    
    
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $proveedor = new Proveedor();            
        $proveedor->find_first("$id");
        if($proveedor->estado ==1){
            $proveedor->estado = "0";
            $estado= "Proveedor Inactivo";            
        }  else {
            $proveedor->estado = "1";     
            $estado= "Proveedor Activo";            
        }
        if ($proveedor->update()){
            Flash::info("$estado");
            return Redirect::toAction('listar');                    
        }
    }
    
    /**
     * Método para subir imágenes
     */
    public function upload() {     
        $upload = new DwUpload('img', 'img/upload/proveedores/');
        $upload->setAllowedTypes('png|jpg|gif|jpeg');
        $upload->setEncryptName(TRUE);
        $upload->setSize('3MB', 400, 400, TRUE);
        if(!$data = $upload->save()) { //retorna un array('path'=>'ruta', 'name'=>'nombre.ext');
            $data = array('error'=>TRUE, 'message'=>$upload->getError());
        }
        sleep(1);//Por la velocidad del script no permite que se actualize el archivo
        $this->data = $data;
        View::select(NULL, 'json');
    }     

}
