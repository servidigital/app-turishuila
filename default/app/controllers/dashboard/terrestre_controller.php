<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category
 * @package     Controllers
 */
Load::models('cliente','vehiculo','planes');  // carga modelos

class TerrestreController extends BackendController {

    public $page_title = 'Principal';
    public $page_module = 'Terrestre';


    public function index($order='order.id.asc', $page='page.1') { 
        $page = (Filter::get($page, 'page') > 0) ? Filter::get($page, 'page') : 1;
        $planes = new Planes();
        $this->planes = $planes->GetListadoplanes($order, $page);
        $this->order = $order;        
        $this->page_title = 'Planes';
    }     

    public function agregar_plan(){
        $this->vehiculos = load::model('vehiculo')->find("conditions: estado=1");

        if(Input::hasPost('planes')) {
            ActiveRecord::beginTrans();
            $planes = new Planes(Input::post('planes'));
            $planes->estado = 1;
            if($planes->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('La Plan se ha creado correctamente.');
                return Redirect::toAction("/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }        
    }


}
