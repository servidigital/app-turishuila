<?php

/**
 * Controller por defecto si no se usa el routes
 * 
 */
class HomeController extends AppController {

    public $page_title = "echo Config::get('config.application.name');";
    
    public function index() {
        Redirect::to('sistema/'); //envio a login        
    }

}
