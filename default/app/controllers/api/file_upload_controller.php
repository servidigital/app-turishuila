<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("visa_adjunto","visa_historial");  // carga modelos

class FileUploadController extends RestController {

    /**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   

        $visa_adjuntos = load::model("visa_adjunto")->find("conditions: visa_id =$id");
        $this->data = array('logger'=>True,'msg'=>"OK","visa_adjuntos"=>$visa_adjuntos);        
    }    

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {        
        $json = $this->param();
        $cliente_id = $json['cliente_id'];
        $visa_id = $json['visa_id'];
        $archivo = $json['archivo'];
        $nombre_archivo = $json['nombre_archivo'];

        $exploded = explode(',',$archivo);
        $decoded = base64_decode($exploded[1]);

        if(strpos($exploded[0],'pdf') == True){
            $extension = 'pdf';
        }

        if(strpos($exploded[0],'png') == True ){
            $extension = 'png';
        }

        if(strpos($exploded[0],'jpeg') == True){
            $extension = 'jpg';            
        }


        $name = md5(time());
        // $fileName= $nombre_archivo.'.'.$extension;
        $fileName= $name.'.'.$extension;

        // $path = PUBLIC_PATH.'default/public/img/'.$fileName;
        // $path = '/Users/avilac3/web/backend-equipo-trabajo/default/public/img/upload/'.$fileName;
        $path = RAIZ.$fileName;
        file_put_contents($path, $decoded);

        $visa_adjunto = New VisaAdjunto();
        $visa_adjunto->cliente_id = $cliente_id;
        $visa_adjunto->visa_id = $visa_id;
        $visa_adjunto->archivo = $fileName;
        $visa_adjunto->nombre_archivo = $nombre_archivo;
        
        $visa_adjunto->estado = 1;
        if($visa_adjunto->save()){
            $visa_historial = New VisaHistorial();
            $visa_historial->visa_id =  $visa_adjunto->visa_id;
            $visa_historial->cliente_id = $visa_adjunto->cliente_id;
            $visa_historial->estado = 1;
            $visa_historial->usuario_id = Session::get('id');
            $visa_historial->detalle = "Carga Archivo <b>$visa_adjunto->nombre_archivo</b>";
            $visa_historial->archivo =  $visa_adjunto->archivo;
            $visa_historial->save();

            $this->data = array('logger'=>True,'filename'=>$fileName,'visa_id'=>"$visa_adjunto->visa_id");
        }else{
            $this->data = array('logger'=>False,'filename'=>$fileName,'visa_id'=>"$visa_adjunto->visa_id");
        }

      }


    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_politica');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
