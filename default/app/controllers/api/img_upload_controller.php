<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

class ImgUploadController extends RestController {


    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */
    public function post() {
        $json = $this->param();
        $id = $json['id'];
        $archivo = $json['archivo'];

        $exploded = explode(',',$archivo);
        $decoded = base64_decode($exploded[1]);

        if(strpos($exploded[0],'jpeg'))
            $extension = 'jpg';
        else
            $extension = 'png';

        $name = md5(time());
        $fileName= $name.'.'.$extension;

        // $path = PUBLIC_PATH.'default/public/img/'.$fileName;
        // $path = '/Users/avilac3/web/backend-equipo-trabajo/default/public/img/upload/'.$fileName;
        $path = RAIZ.$fileName;
        file_put_contents($path, $decoded);

        $this->data = array('logger'=>True,'filename'=>$fileName);

            // $file = Upload::factory('archivo');//llamamos a la libreria y le pasamos el nombre del campo file del formulario

    }

} // fin controller
