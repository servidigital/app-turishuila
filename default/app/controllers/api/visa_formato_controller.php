<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("visa_formato");  // carga modelos

class VisaFormatoController extends RestController {

    public $model = 'visa_formato';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   
        // $cotizacion_politicas = load::model("cotizacion_politica")->find("conditions: cotizacion_id =$id");

        $cotizacion_politicas = load::model("cotizacion_politica")->find('columns: cotizacion_politica.cotizacion_id, politica.nombre, cotizacion_politica.id as cotizacion_politica_id',
                           'join: inner join politica on cotizacion_politica.politica_id = politica.id',"conditions: cotizacion_politica.cotizacion_id = $id");

        $this->data = array('logger'=>True,'msg'=>"OK",'politicas'=>$cotizacion_politicas,'id'=>$id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $json = $this->param(); 
        $campos = $json['visa_formato'];                         
        
        $visa_formato = load::model("visa_formato");
        $visa_formato->nombre = $campos['nombre'];
        $visa_formato->requisitos = $campos['requisitos'];
        $visa_formato->costo = $campos['costo'];
        $visa_formato->formulario = $campos['formulario'];
        $visa_formato->costo_servicio = $campos['costo_servicio'];
        $visa_formato->img = $campos['img'];
        $visa_formato->estado = 1;
        if($visa_formato->save()){
            $this->data = array('logger'=>True,'msg'=>"OK");        
        }else{
            $this->data = array('logger'=>False,'msg'=>"ERROR");        
        }
      }


    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
        $json = $this->param(); 
        $editVisaFormato = $json['editVisaFormato'];

        $obj = Load::model('visa_formato');
        $obj = $obj->find_first((int) $id);
        $obj->nombre = $editVisaFormato['nombre'];
        $obj->requisitos = $editVisaFormato['requisitos'];
        $obj->costo = $editVisaFormato['costo'];
        $obj->costo_servicio = $editVisaFormato['costo_servicio'];
        $obj->formulario = $editVisaFormato['formulario'];
        $obj->img = $editVisaFormato['img'];

        if ($obj->update($this->param())) {
            $this->setCode(202);
            $ok_data = array('logger'=>True,'msg'=>'Actualizado Exitosamente');                
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }       
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_politica');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
