<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("producto");  // carga modelos

class ProductosController extends RestController {

    public $model = 'producto';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {      
        $this->data = Load::model($this->model)->find((int) $id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $nombre = $json['nombre'];
        $celular = $json['celular'];
        $email = $json['email'];

        $crear_cliente = new Cliente();
        $crear_cliente->nombre = strtoupper("$nombre");
        $crear_cliente->celular = "$celular";
        $crear_cliente->email = strtoupper("$email");
        $crear_cliente->estado=1;

         if($crear_cliente->save()){
            $ok_data = array('logger'=>True,'msg'=>"Cliente ");
            $this->data = $ok_data;
         }else{
            $error_data = array('logger'=>False,'msg'=>'Error Cliente');
            $this->data = $error_data;                
         }                  
         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
    $token_app_core = APP_TOKEN_CORE;

        if($token_app_core ==$token_app_core){

            $obj = Load::model($this->model);
            $obj = $obj->find_first((int) $id);
            if ($obj->update($this->param())) {
                $this->setCode(202);
                $ok_data = array('logger'=>True,'msg'=>'Cliente Actualizado Exitosamente','clientes'=>$obj);                
                $this->data = $ok_data;
            } else {
                $error_data = array('logger'=>False,'msg'=>'Error');
                $this->data = $error_data;
            }

        }else{
            $error_data = array('logger'=>False,'msg'=>'Error de Token'.$token_app);
            $this->data = $error_data;
        }         
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model($this->model);
        $obj = $obj->find_first((int) $id);
        $obj->estado = -1;                
        if ($obj->update($this->param())) {
            $this->setCode(200);
            $this->data = $obj;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
