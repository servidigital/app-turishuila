<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("cotizacion_plantilla");  // carga modelos

class CotizacionPlantillaController extends RestController {

    public $model = 'cotizacion_plantilla';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   
        // $cotizacion_politicas = load::model("cotizacion_politica")->find("conditions: cotizacion_id =$id");

        $cotizacion_politicas = load::model("cotizacion_politica")->find('columns: cotizacion_politica.cotizacion_id, politica.nombre, cotizacion_politica.id as cotizacion_politica_id',
                           'join: inner join politica on cotizacion_politica.politica_id = politica.id',"conditions: cotizacion_politica.cotizacion_id = $id");

        $this->data = array('logger'=>True,'msg'=>"OK",'politicas'=>$cotizacion_politicas,'id'=>$id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $json = $this->param(); 
        $plantilla = $json['plantilla'];                                 
        $cotizacion_plantilla = load::model("cotizacion_plantilla");
        $cotizacion_plantilla->codigo = $plantilla['codigo'];
        $cotizacion_plantilla->nombre = $plantilla['nombre'];
        $cotizacion_plantilla->descripcion = $plantilla['descripcion'];
        $cotizacion_plantilla->estado = 1;
        if($cotizacion_plantilla->save()){
            $this->data = array('logger'=>True,'msg'=>"OK");        
        }else{
            $this->data = array('logger'=>False,'msg'=>"ERROR");        
        }
      }


    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
        $json = $this->param(); 
        $plantilla = $json['plantilla']; 
        
        $obj = Load::model('cotizacion_plantilla');
        $obj = $obj->find_first((int) $id);                              
        $obj->codigo = $plantilla['codigo'];
        $obj->nombre = $plantilla['nombre'];
        $obj->descripcion = $plantilla['descripcion'];
        // $obj->estado = 1;
        if($obj->update()){
            $this->data = array('logger'=>True,'msg'=>"OK");        
        }else{
            $this->data = array('logger'=>False,'msg'=>"ERROR");        
        }        
      
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_plantilla');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
