<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("trm");  // carga modelos

class TrmController extends RestController {

    public $model = 'trm';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {       
        $data = Load::model($this->model)->find("conditions: estado >=$id","order: id desc");        
        $this->data = $data;        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $data = Load::model($this->model)->find();
        $trmActual = Load::model($this->model)->find_first("order: id desc","conditions: moneda='USD'");
        // $this->data = $data;
        $ok_data = array('logger'=>True,'msg'=>"TRM",'trm'=>$data,'trmActual'=>$trmActual);
        $this->data = $ok_data;

    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $trm = $json['trm'];

        $crear = new Trm();
        $crear->moneda = $trm['moneda'];
        $crear->valor = $trm['valor'];

         if($crear->save()){
            $this->data = array('logger'=>True,'msg',$trm);
         }else{
            $this->data = array('logger'=>False,'msg'=>'Error',$trm);                
         }                  
         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $plan = $json['plan'];
        $plan_item_costeo = $plan['plan_item_costeo'];
        // $costeo = $json['costeo'];


        $obj = $obj->find_first((int) $id);
        $obj->nombre = $plan['nombre'];
        $obj->descripcion = $plan['descripcion'];
        $obj->vehiculo_id = $plan['vehiculo_id'];
        $obj->fecha_out = $plan['fecha_out'];
        $obj->valor = $plan['valor'];
        $obj->img1 = $plan['img1'];
        $obj->valor_bus = $plan['valor_bus'];
        $obj->ingreso_propio = $plan['ingreso_propio'];
        $obj->gastos_financieros = $plan['gastos_financieros'];
        $obj->valor_adulto = $plan['valor_adulto'];
        $obj->valor_nino = $plan['valor_nino'];

        $obj->estado=$plan['estado'];

        if($obj->update()){
            if($obj->estado == 2){
                $vehiculoPlantilla = load::model('vehiculo_plantilla')->find("conditions: vehiculo_id=$obj->vehiculo_id");
                foreach ($vehiculoPlantilla as $itemPlantilla) {
                    $planVehiculoPlantilla = new PlanVehiculoPlantilla();
                    $planVehiculoPlantilla->vehiculo_id = $itemPlantilla->vehiculo_id;
                    $planVehiculoPlantilla->tipo = $itemPlantilla->tipo;                    
                    $planVehiculoPlantilla->silla = $itemPlantilla->silla;
                    $planVehiculoPlantilla->save();
                }
            }

            // delete plan Item Costeo
            $plan_item_costeo_delete = load::model('plan_item_costeo')->find("conditions: plan_id=$obj->id");
            foreach ($plan_item_costeo_delete as $itemCosteo) {
                $itemCosteo->delete("plan_id=$obj->id");
            }

            // Guardo Item Costeo
            foreach ($plan_item_costeo as $itemCosteoNew) {
                $plan_item_costeo = new PlanItemCosteo();
                $plan_item_costeo->plan_id = $obj->id;
                $plan_item_costeo->detalle = $itemCosteoNew['detalle'];
                $plan_item_costeo->cantidad = $itemCosteoNew['cantidad'];
                $plan_item_costeo->valor_adulto = $itemCosteoNew['valor_adulto'];
                $plan_item_costeo->valor_nino = $itemCosteoNew['valor_nino'];
                $plan_item_costeo->save();

            }

            $ok_data = array('logger'=>True,'msg'=>"Plan",$plan_item_costeo_delete);
            $this->data = $ok_data;
        }else{
            $error_data = array('logger'=>False,'msg'=>'Error Plan');
            $this->data = $error_data;                
        }        
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        // $obj = Load::model($this->model);
        // $obj = $obj->find_first((int) $id);
        // $obj->estado = -1;                
        // if ($obj->update($this->param())) {
        //     $this->setCode(200);
        //     $this->data = $obj;
        // } else {
        //     $error_data = array('logger'=>False,'msg'=>'Error');
        //     $this->data = $error_data;
        // }

    }

}
