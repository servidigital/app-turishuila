<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

// Load::models("visa_formato");  // carga modelos

class AdjuntarArchivoController extends RestController {

    // public $model = 'visa_formato';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   
        // $cotizacion_politicas = load::model("cotizacion_politica")->find("conditions: cotizacion_id =$id");

        // $cotizacion_politicas = load::model("cotizacion_politica")->find('columns: cotizacion_politica.cotizacion_id, politica.nombre, cotizacion_politica.id as cotizacion_politica_id',
        //                    'join: inner join politica on cotizacion_politica.politica_id = politica.id',"conditions: cotizacion_politica.cotizacion_id = $id");

        // $this->data = array('logger'=>True,'msg'=>"OK",'politicas'=>$cotizacion_politicas,'id'=>$id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        // $this->data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {        
        $json = $this->param();
        $id = $json['id'];
        $archivo = $json['archivo'];

        $exploded = explode(',',$archivo);
        $decoded = base64_decode($exploded[1]);

        if(strpos($exploded[0],'pdf'))
            $extension = 'pdf';
        else
            $extension = 'pdf';

        $name = md5(time());
        $fileName= $name.'.'.$extension;

        // $path = PUBLIC_PATH.'default/public/img/'.$fileName;
        // $path = '/Users/avilac3/web/backend-equipo-trabajo/default/public/img/upload/'.$fileName;
        $path = RAIZ.$fileName;
        file_put_contents($path, $decoded);

        $this->data = array('logger'=>True,'filename'=>$fileName);
      }


    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
      
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_politica');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
