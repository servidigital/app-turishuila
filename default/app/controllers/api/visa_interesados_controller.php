<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("visa");  // carga modelos
Load::lib('bitly'); // cargo lib

class VisaInteresadosController extends RestController {

    public $model = 'visa';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   
        // $cotizacion_politicas = load::model("cotizacion_politica")->find("conditions: cotizacion_id =$id");

        $cotizacion_politicas = load::model("cotizacion_politica")->find('columns: cotizacion_politica.cotizacion_id, politica.nombre, cotizacion_politica.id as cotizacion_politica_id',
                           'join: inner join politica on cotizacion_politica.politica_id = politica.id',"conditions: cotizacion_politica.cotizacion_id = $id");

        $this->data = array('logger'=>True,'msg'=>"OK",'politicas'=>$cotizacion_politicas,'id'=>$id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $visas = Load::model($this->model)->find("order: id desc","conditions: estado=1 AND estado_solicitud IS NULL");
         
        foreach ($visas as $visa) {
            $visa_formato = load::model('visa_formato')->find_first("conditions: id=$visa->visa_formato_id");
            $visa->img =  $visa_formato->img;
            $visa->visa_nombre =  $visa_formato->nombre;

            $cliente = load::model('cliente')->find_first("conditions: id=$visa->cliente_id");
            $visa->nombre = $cliente->nombre;
            $visa->cliente_celular = $cliente->celular;

            $visa_historial = load::model('visa_historial')->find("conditions: cliente_id=$visa->cliente_id","order: id desc");
            foreach ($visa_historial as $vHistorial) {
                $usuario = load::model('sistema/usuario')->find_first("conditions: id=$vHistorial->usuario_id");
                $vHistorial->usuario = $usuario->nombre;    
            }
            $visa->historial = $visa_historial;

            $usuario = load::model('sistema/usuario')->find_first("conditions: id=$visa->usuario_id");
            $visa->usuario = $usuario->nombre;
        }
        // $this->data = array('logger'=>True,'msg'=>"OK",'visas'=>$visas);        
        $this->data = $visas;        

    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $json = $this->param(); 
        $visa_id = $json['visa_id'];                         
        $cliente_id = $json['cliente_id'];                         
        $usuario_id = $json['usuario_id'];                         
        $valor_servicio = $json['valor_servicio'];                         
        
        $crear_visa = load::model("visa");
        $crear_visa->cliente_id = $cliente_id;
        $crear_visa->visa_formato_id = $visa_id;
        $crear_visa->usuario_id = $usuario_id; 
        $crear_visa->valor_servicio = $valor_servicio; 
        $crear_visa->token = md5(time());
        $crear_visa->estado = 1;

        if($crear_visa->save()){

            // ACORTADOR DE URL
            $dominio  = DOMINIO.'reporte/visa/ver/'.$crear_visa->token;
            $params = array();
            $params['access_token'] = 'a6bbdaa7dc961c56962810ea00c9b33e6bb90ab1';
            // $params['longUrl'] = 'https://th.faroapp.com.co/reporte/visa/ver/'.$crear_visa->token;
            $params['longUrl'] = $dominio;
            $params['domain'] = 'j.mp';
            $results = bitly_get('shorten', $params);
            $results_data = $results['data'];
            $crear_visa->url_corta = $results_data['url'];            
            $crear_visa->update();

            $this->data = array('logger'=>True,'msg'=>"OK");        
        }else{
            $this->data = array('logger'=>False,'msg'=>"ERROR");        
        }
      }


    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
        $json = $this->param(); 
        $editVisa = $json['editVisa'];

        $obj = Load::model('visa');
        $obj = $obj->find_first((int) $id);
        $obj->foto = $editVisa['foto'];
        $obj->pasaporte = $editVisa['pasaporte'];
        $obj->formulario = $editVisa['formulario'];
        $obj->derecho_consular = $editVisa['derecho_consular'];
        $obj->fecha_cita1 = $editVisa['fecha_cita1'];
        $obj->fecha_cita2 = $editVisa['fecha_cita2'];
        $obj->personas = $editVisa['personas'];
        $obj->valor_servicio = $editVisa['valor_servicio'];
        $obj->estado_solicitud = $editVisa['estado_solicitud'];
        if($editVisa['estado_solicitud'] >= 4){
            $obj->estado = '0';
        }else{
            $obj->estado = $editVisa['estado'];
        }

        if ($obj->update($this->param())) {
            $this->setCode(202);
            $ok_data = array('logger'=>True,'msg'=>'Actualizado Exitosamente');                
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }      
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_politica');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
