<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("cliente",'cotizacion');  // carga modelos
Load::lib('bitly'); // cargo lib

class CotizacionController extends RestController {

    public $model = 'cliente';

    /**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {    
        $cotizacion = load::model("cotizacion")->find_first("conditions: id=$id");
        $cotizaciones_item = load::model('cotizacion_item')->find("conditions: cotizacion_id=$id");
            foreach($cotizaciones_item as $item){
                $producto = load::model('producto')->find_first("conditions: id=$item->producto_id");
                $item->icono = $producto->icono;

                $cotizacion_item_precio = load::model('cotizacion_item_precio')->find("conditions: cotizacion_id=$id");
                $item->cotizacion_item_precio = $cotizacion_item_precio;
            }

        $this->data = array('logger'=>True,'msg'=>"OK",'cotizacion'=>$cotizacion,'cotizaciones_item'=>$cotizaciones_item,'cotizacion_item_precio'=>$cotizacion_item_precio);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

    public function post() {
        $json = $this->param(); 
        $cliente_id = $json['cliente_id'];
        $estado = $json['estado'];
        $campos = !empty($json['cotizacion_item']) ? $json['cotizacion_item'] : '';
        $this->data = array('logger'=>True,'msg'=>'Post');                
    }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $json = $this->param(); 
        $cotizacion = $json['cotizacion'];

        $obj = Load::model('cotizacion');
        // $id = $cotizacion['id'];
        $obj = $obj->find_first((int) $id);
        $obj->vencimiento = $cotizacion['vencimiento'];
        if ($obj->update($this->param())) {
          $this->setCode(202);
          $this->data = array('logger'=>True,'msg'=>'Actualizado');                
        } else {
          $this->data = array('logger'=>False,'msg'=>'Error');
        }
         
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model($this->model);
        $obj = $obj->find_first((int) $id);
        $obj->estado = -1;                
        if ($obj->update($this->param())) {
            $this->setCode(200);
            $this->data = $obj;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
