<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("codigo_iata");  // carga modelos

class IataController extends RestController {

    public $model = 'codigo_iata';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {       
        $this->data = Load::model($this->model)->find((int) $id);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: ciudad asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
                         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
      
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {


    }

}
