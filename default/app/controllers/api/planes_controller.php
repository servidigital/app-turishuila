<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("cliente","plan_vehiculo_plantilla","plan_item_costeo");  // carga modelos

class PlanesController extends RestController {

    public $model = 'planes';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {       
        $data = Load::model($this->model)->find("conditions: estado >=$id","order: id desc");        
        foreach ($data as $item) {
            $vehiculo = load::model('vehiculo')->find_first("conditions: id=$item->vehiculo_id");
            $item->vehiculo_nombre = $vehiculo->nombre;
            $item->vehiculo_sillas = $vehiculo->sillas;

            $plan_vehiculo_plantilla = load::model('plan_vehiculo_plantilla')->find("conditions: vehiculo_id=$item->vehiculo_id");
            $item->plan_vehiculo_plantilla = $plan_vehiculo_plantilla;
            
            $plan_costeo_balance = load::model('plan_costeo_balance')->find("conditions: plan_id=$item->id");
            $item->plan_costeo_balance = $plan_costeo_balance;

            $plan_item_costeo = load::model('plan_item_costeo')->find("conditions: plan_id=$item->id");
            $item->plan_item_costeo = $plan_item_costeo;

            $plan_pasajeros = load::model('plan_reserva_item')->find("conditions: plan_id=$item->id");
            $item->listado_pasajeros = $plan_pasajeros;


            $plan_costeo_balance = load::model('plan_costeo_balance')->find("conditions: plan_id=$item->id");
            $item->listado_costeo_balance = $plan_costeo_balance;


            $plan_costeo_balance = load::model('plan_costeo_balance')->sum("valor","conditions: plan_id=$item->id");
            $item->totalOperativo = $plan_costeo_balance;

            $plan_reserva_abono_total = load::model('plan_reserva_abono')->sum("valor","conditions: plan_id=$item->id");
            $item->totalVenta = $plan_reserva_abono_total;

            $item->totalUtilidad = $plan_reserva_abono_total - $plan_costeo_balance;

        }
        $this->data = $data;        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $data = Load::model('planes')->find("order: nombre asc","conditions: estado=1");
        foreach ($data as $item) {

            $vehiculo = load::model('vehiculo')->find_first("conditions: id=$item->vehiculo_id");
            $item->vehiculo_nombre = $vehiculo->nombre;

            // $plan_costeo_balance = load::model('plan_costeo_balance')->sum("valor","conditions: plan_id=$item->id");
            // $item->totalOperativo = $plan_costeo_balance;

            // $plan_reserva_abono_total = load::model('plan_reserva_abono')->sum("valor","conditions: plan_reserva_id=$item->id");
            // $item->totalVenta = $plan_reserva_abono_total;

            $plan_item_costeo = load::model('plan_item_costeo')->find("conditions: plan_id=$item->id");
            foreach($plan_item_costeo as $itemCosteoPlan){
                $itemCosteoPlan->valor_adulto = number_format($itemCosteoPlan->valor_adulto, 3);
                $itemCosteoPlan->valor_nino = number_format($itemCosteoPlan->valor_nino, 3);
            }
            $item->plan_item_costeo = $plan_item_costeo;  
          
        }
        $this->data = $data;
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $plan = $json['plan'];
        $costeo = $json['costeo'];

        $crear = new Planes();
        $crear->nombre = $plan['nombre'];
        $crear->descripcion = $plan['descripcion'];
        $crear->vehiculo_id = $plan['vehiculo_id'];
        $crear->fecha_out = $plan['fecha_out'];
        $crear->valor = $plan['valor'];
        $crear->img1 = $plan['img1'];
        $crear->valor_bus = $plan['valor_bus'];
        $crear->ingreso_propio = $plan['ingreso_propio'];
        $crear->gastos_financieros = $plan['gastos_financieros'];
        $crear->valor_adulto = $plan['valor_adulto'];
        $crear->valor_nino = $plan['valor_nino'];
        $crear->estado=1;

         if($crear->save()){
             foreach ($costeo as $itemCosteo) {
                 $plan_item_costeo = load::model('plan_item_costeo');
                 $plan_item_costeo->plan_id = $crear->id;
                 $plan_item_costeo->detalle = $itemCosteo['detalle'];
                 $plan_item_costeo->cantidad = $itemCosteo['cantidad'];
                 $plan_item_costeo->valor_adulto = $itemCosteo['valor_adulto'];
                 $plan_item_costeo->valor_nino = $itemCosteo['valor_nino'];
                 $plan_item_costeo->save();
             }
            $ok_data = array('logger'=>True,'msg'=>"Plan",$costeo);
            $this->data = $ok_data;
         }else{
            $error_data = array('logger'=>False,'msg'=>'Error Plan');
            $this->data = $error_data;                
         }                  
         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $plan = $json['plan'];
        $plan_item_costeo = $plan['plan_item_costeo'];
        // $costeo = $json['costeo'];


        $obj = $obj->find_first((int) $id);
        $obj->nombre = $plan['nombre'];
        $obj->descripcion = $plan['descripcion'];
        $obj->vehiculo_id = $plan['vehiculo_id'];
        $obj->fecha_out = $plan['fecha_out'];
        $obj->valor = $plan['valor'];
        $obj->img1 = $plan['img1'];
        $obj->valor_bus = $plan['valor_bus'];
        $obj->ingreso_propio = $plan['ingreso_propio'];
        $obj->gastos_financieros = $plan['gastos_financieros'];
        $obj->valor_adulto = $plan['valor_adulto'];
        $obj->valor_nino = $plan['valor_nino'];

        $obj->estado=$plan['estado'];

        if($obj->update()){
            if($obj->estado == 2){
                $vehiculoPlantilla = load::model('vehiculo_plantilla')->find("conditions: vehiculo_id=$obj->vehiculo_id");
                foreach ($vehiculoPlantilla as $itemPlantilla) {
                    $planVehiculoPlantilla = new PlanVehiculoPlantilla();
                    $planVehiculoPlantilla->vehiculo_id = $itemPlantilla->vehiculo_id;
                    $planVehiculoPlantilla->tipo = $itemPlantilla->tipo;                    
                    $planVehiculoPlantilla->silla = $itemPlantilla->silla;
                    $planVehiculoPlantilla->save();
                }
            }

            // delete plan Item Costeo
            $plan_item_costeo_delete = load::model('plan_item_costeo')->find("conditions: plan_id=$obj->id");
            foreach ($plan_item_costeo_delete as $itemCosteo) {
                $itemCosteo->delete("plan_id=$obj->id");
            }

            // Guardo Item Costeo
            foreach ($plan_item_costeo as $itemCosteoNew) {
                $plan_item_costeo = new PlanItemCosteo();
                $plan_item_costeo->plan_id = $obj->id;
                $plan_item_costeo->detalle = $itemCosteoNew['detalle'];
                $plan_item_costeo->cantidad = $itemCosteoNew['cantidad'];
                $plan_item_costeo->valor_adulto = $itemCosteoNew['valor_adulto'];
                $plan_item_costeo->valor_nino = $itemCosteoNew['valor_nino'];
                $plan_item_costeo->save();

            }

            $ok_data = array('logger'=>True,'msg'=>"Plan",$plan_item_costeo_delete);
            $this->data = $ok_data;
        }else{
            $error_data = array('logger'=>False,'msg'=>'Error Plan');
            $this->data = $error_data;                
        }        
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        // $obj = Load::model($this->model);
        // $obj = $obj->find_first((int) $id);
        // $obj->estado = -1;                
        // if ($obj->update($this->param())) {
        //     $this->setCode(200);
        //     $this->data = $obj;
        // } else {
        //     $error_data = array('logger'=>False,'msg'=>'Error');
        //     $this->data = $error_data;
        // }

    }

}
