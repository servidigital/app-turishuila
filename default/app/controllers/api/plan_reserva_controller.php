<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("plan_reserva","plan_reserva_item","plan_reserva_abono");  // carga modelos

class PlanReservaController extends RestController {

    public $model = 'plan_reserva';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {       
        $data = Load::model($this->model)->find("conditions: plan_id =$id","order: id desc");        
        foreach ($data as $item) {
            $plan = load::model('planes')->find_first("conditions: id=$item->plan_id");
            $item->plan_nombre = $plan->nombre;

            $cliente = load::model('cliente')->find_first("conditions: id=$item->cliente_id");
            $item->cliente = $cliente->nombre;


            $personasCounter = load::model('plan_reserva_item')->count("plan_reserva_id=$item->id");
            $item->personasCounter = $personasCounter;

            $reserva_item = load::model('plan_reserva_item')->find("conditions: plan_reserva_id=$item->id");
            foreach($reserva_item as $itemReserva){
                $plan_vehiculo_plantilla = load::model('plan_vehiculo_plantilla')->find_first("id=$itemReserva->plan_vehiculo_plantilla_id");
                $itemReserva->silla = $plan_vehiculo_plantilla->silla;
            }
            $item->reserva_item = $reserva_item;

            $plan_reserva_abono = load::model('plan_reserva_abono')->find("conditions: plan_reserva_id=$item->id");
            $item->plan_reserva_abono = $plan_reserva_abono; 
            
            $plan_reserva_abono_total = load::model('plan_reserva_abono')->sum("valor","conditions: plan_reserva_id=$item->id");
            $item->saldo = $item->total - $plan_reserva_abono_total;

        }
        $this->data = $data;        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $data = Load::model('planes')->find("order: nombre asc","conditions: estado=1");
        foreach ($data as $item) {

            $vehiculo = load::model('vehiculo')->find_first("conditions: id=$item->vehiculo_id");
            $item->vehiculo_nombre = $vehiculo->nombre;

            $plan_item_costeo = load::model('plan_item_costeo')->find("conditions: plan_id=$item->id");
            foreach($plan_item_costeo as $itemCosteoPlan){
                $itemCosteoPlan->valor_adulto = number_format($itemCosteoPlan->valor_adulto, 3);
                $itemCosteoPlan->valor_nino = number_format($itemCosteoPlan->valor_nino, 3);
            }
            $item->plan_item_costeo = $plan_item_costeo;  
          
        }
        $this->data = $data;
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $clienteId = $json['clienteId'];
        $planId = $json['planId'];
        $reserva = $json['reservaItem'];
        $acomodacion = $json['acomodacion'];
        $saldo = $json['saldo'];
        $total = $json['total'];

        $crear = new PlanReserva();
        $crear->plan_id = $planId;
        $crear->cliente_id = $clienteId;
        $crear->acomodacion = $acomodacion;
        $crear->total = $total;
        $crear->saldo = $saldo;
        $crear->estado=1;

         if($crear->save()){
             foreach ($reserva as $itemReserva) {
                 $plan_reserva_item = load::model('plan_reserva_item');
                 $plan_reserva_item->plan_id = $planId;
                 $plan_reserva_item->plan_reserva_id = $crear->id;
                 $plan_reserva_item->plan_vehiculo_plantilla_id = $itemReserva['idPlantilla'];
                 $plan_reserva_item->nombre = $itemReserva['nombre'];
                 $plan_reserva_item->documento = $itemReserva['documento'];
                 $plan_reserva_item->nacimiento = $itemReserva['nacimiento'];
                 $plan_reserva_item->tipo = $itemReserva['tipo'];
                 $plan_reserva_item->silla = $itemReserva['silla'];
                 $plan_reserva_item->valor = $itemReserva['valor'];
                 $plan_reserva_item->estado = 1;
                 if($plan_reserva_item->save()){
                     $plan_vehiculo_plantilla = load::model('plan_vehiculo_plantilla');
                     $plan_vehiculo_plantilla = $plan_vehiculo_plantilla->find_first("conditions: id = $plan_reserva_item->plan_vehiculo_plantilla_id");
                     $plan_vehiculo_plantilla->estado = 1;
                     $plan_vehiculo_plantilla->update();
                 }
             }
            $ok_data = array('logger'=>True,'msg'=>"Plan");
            $this->data = $ok_data;
         }else{
            $error_data = array('logger'=>False,'msg'=>'Error Plan');
            $this->data = $error_data;                
         }                  
         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $plan = $json['plan'];
        $plan_item_costeo = $plan['plan_item_costeo'];
        // $costeo = $json['costeo'];


        $obj = $obj->find_first((int) $id);
        $obj->nombre = $plan['nombre'];
        $obj->descripcion = $plan['descripcion'];
        $obj->vehiculo_id = $plan['vehiculo_id'];
        $obj->fecha_out = $plan['fecha_out'];
        $obj->valor = $plan['valor'];
        $obj->img1 = $plan['img1'];
        $obj->valor_bus = $plan['valor_bus'];
        $obj->ingreso_propio = $plan['ingreso_propio'];
        $obj->gastos_financieros = $plan['gastos_financieros'];
        $obj->valor_adulto = $plan['valor_adulto'];
        $obj->valor_nino = $plan['valor_nino'];

        $obj->estado=$plan['estado'];

        if($obj->update()){
            if($obj->estado == 2){
                $vehiculoPlantilla = load::model('vehiculo_plantilla')->find("conditions: vehiculo_id=$obj->vehiculo_id");
                foreach ($vehiculoPlantilla as $itemPlantilla) {
                    $planVehiculoPlantilla = new PlanVehiculoPlantilla();
                    $planVehiculoPlantilla->vehiculo_id = $itemPlantilla->vehiculo_id;
                    $planVehiculoPlantilla->tipo = $itemPlantilla->tipo;                    
                    $planVehiculoPlantilla->silla = $itemPlantilla->silla;
                    $planVehiculoPlantilla->save();
                }
            }

            // delete plan Item Costeo
            $plan_item_costeo_delete = load::model('plan_item_costeo')->find("conditions: plan_id=$obj->id");
            foreach ($plan_item_costeo_delete as $itemCosteo) {
                $itemCosteo->delete("plan_id=$obj->id");
            }

            // Guardo Item Costeo
            foreach ($plan_item_costeo as $itemCosteoNew) {
                $plan_item_costeo = new PlanItemCosteo();
                $plan_item_costeo->plan_id = $obj->id;
                $plan_item_costeo->detalle = $itemCosteoNew['detalle'];
                $plan_item_costeo->cantidad = $itemCosteoNew['cantidad'];
                $plan_item_costeo->valor_adulto = $itemCosteoNew['valor_adulto'];
                $plan_item_costeo->valor_nino = $itemCosteoNew['valor_nino'];
                $plan_item_costeo->save();

            }

            $ok_data = array('logger'=>True,'msg'=>"Plan",$plan_item_costeo_delete);
            $this->data = $ok_data;
        }else{
            $error_data = array('logger'=>False,'msg'=>'Error Plan');
            $this->data = $error_data;                
        }        
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        // $obj = Load::model($this->model);
        // $obj = $obj->find_first((int) $id);
        // $obj->estado = -1;                
        // if ($obj->update($this->param())) {
        //     $this->setCode(200);
        //     $this->data = $obj;
        // } else {
        //     $error_data = array('logger'=>False,'msg'=>'Error');
        //     $this->data = $error_data;
        // }

    }

}
