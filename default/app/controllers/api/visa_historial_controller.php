<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("visa_historial");  // carga modelos

class VisaHistorialController extends RestController {

    public $model = 'visa_historial';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {   
        $visa_historial = load::model('visa_historial')->find("conditions: visa_id=$id");        
        $this->data = array('logger'=>True,'msg'=>"OK",'visa_historial'=>$visa_historial);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $this->data = Load::model($this->model)->find("order: id asc","conditions: estado=1");
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $json = $this->param(); 
        $campos = $json['visa_historial'];                         
        
        $crear = load::model("visa_historial");
        $crear->detalle = $campos['detalle'];
        $crear->visa_id = $campos['visa_id'];
        $crear->cliente_id = $campos['cliente_id'];
        $crear->usuario_id = $campos['usuario_id'];
        $crear->estado = 1;
        if($crear->save()){
            $this->data = array('logger'=>True,'msg'=>"OK");        
        }else{
            $this->data = array('logger'=>False,'msg'=>"ERROR");        
        }
      }


    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
      
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model('cotizacion_politica');
        $obj = $obj->find_first((int) $id);
        if ($obj->delete($this->param())) {
            $this->setCode(200);
            $ok_data = array('logger'=>True,'msg'=>'Eliminado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
