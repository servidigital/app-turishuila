<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("solicitud","cliente");  // carga modelos

class SolicitudesController extends RestController {

    public $model = 'solicitud';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {
//      $this->data = load::model('cliente')->find_first($id);
//      header("Content-type: application/json");
//      echo json_encode($data);        
        $this->data = Load::model($this->model)->find((int) $id);
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $data = Load::model($this->model)->find("order: id desc","conditions: estado=1");
        foreach ($data as $infoCliente) {
          $cliente = load::model('cliente')->find_first("conditions: id=$infoCliente->cliente_id");
          $infoCliente->nombre = $cliente->nombre;
          $infoCliente->celular = $cliente->celular;
          $infoCliente->email = $cliente->email;
        }
        foreach ($data as $infoUsuario) {
          $usuario = load::model('sistema/usuario')->find_first("conditions: id=$infoUsuario->usuario_id");
          $infoUsuario->usuario_nombre = $usuario->nombre;
        }

        $this->data = $data;
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $detalle = $json['detalle'];
        $color = $json['color'];
        $cliente_id = $json['cliente_id'];
        $usuario_id = $json['usuario_id'];

        $crear_solicitud = new Solicitud();
        $crear_solicitud->detalle = strtoupper("$detalle");
        $crear_solicitud->color = strtoupper("$color");
        $crear_solicitud->usuario_id = $usuario_id;
        $crear_solicitud->cliente_id = $cliente_id;

         if($crear_solicitud->save()){
            $ok_data = array('logger'=>True,'msg'=>"Solicitud ");
            $this->data = $ok_data;
         }else{
            $error_data = array('logger'=>False,'msg'=>'Error Solicitud');
            $this->data = $error_data;                
         }                  
         
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
    $token_app_core = APP_TOKEN_CORE;

        if($token_app_core ==$token_app_core){

            $obj = Load::model($this->model);
            $obj = $obj->find_first((int) $id);
            if ($obj->update($this->param())) {
                $this->setCode(202);
                $ok_data = array('logger'=>True,'msg'=>'Cliente Actualizado Exitosamente','clientes'=>$obj);                
                $this->data = $ok_data;
            } else {
                $error_data = array('logger'=>False,'msg'=>'Error');
                $this->data = $error_data;
            }

        }else{
            $error_data = array('logger'=>False,'msg'=>'Error de Token');
            $this->data = $error_data;
        }         
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model($this->model);
        $obj = $obj->find_first((int) $id);
        $obj->estado = -1;                
        if ($obj->update($this->param())) {
            $this->setCode(200);
            $this->data = $obj;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
