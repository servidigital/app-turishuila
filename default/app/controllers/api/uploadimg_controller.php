<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */


class UploadimgController extends RestController {


    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */
    public function post() {



        if(isset($_FILES))
        {
            if(!$_FILES['file']['error'])
            {
                if(preg_match("/image/", $_FILES['file']['type']))
                {
                    // $name = md5(rand(100, 200));
                    $name = md5(time());
                    $ext = explode('.', $_FILES['file']['name']);
                    $filename = $name . '.' . $ext[1];
                    $destination = RAIZ.'imgcotizaciones/'.$filename;                
                    // $destination = '/Users/avilac3/web/app-turishuila/default/public/files/imgcotizaciones/' . $filename;
                    $location = $_FILES["file"]["tmp_name"];
                    move_uploaded_file($location, $destination);
                    $message['url'] = DOMINIO.'default/public/img/upload/imgcotizaciones/' . $filename; 
                    $this->data = array('url'=>$message['url']);

                }
                else
                {
                    $message['error'] = 'El tipo de archivo no es una imagen';
                }
            }
            else
            {
                $message['error'] = "La imagen no se ha subido correctamente error(".$_FILES['file']['error'].")";
            }	
        }
        else
        {
            $message['error'] = "No se ha enviado ningun archivo";
        }
    

    }

} // fin controller
