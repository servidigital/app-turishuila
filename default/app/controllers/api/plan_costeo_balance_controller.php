<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("plan_costeo_balance");  // carga modelos

class PlanCosteoBalanceController extends RestController {

    public $model = 'plan_costeo_balance';

/**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {       
        $data = Load::model($this->model)->find("conditions: plan_id =$id","order: id desc");        
        $this->data = $data;        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $ok_data = array('logger'=>True,'msg'=>"PlanCosteoBalance");
        $this->data = $ok_data;
}

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        // $obj = Load::model($this->model);   
        // $json = $this->param(); 
        // $clienteId = $json['clienteId'];
        // $planId = $json['planId'];
        // $reserva = $json['reservaItem'];
        // $acomodacion = $json['acomodacion'];
        // $saldo = $json['saldo'];
        // $total = $json['total'];

        // $crear = new PlanCosteoBalance();
        // $crear->plan_id = $planId;
        // $crear->cliente_id = $clienteId;
        // $crear->acomodacion = $acomodacion;
        // $crear->total = $total;
        // $crear->saldo = $saldo;
        // $crear->estado=1;

        //  if($crear->save()){
        //     $ok_data = array('logger'=>True,'msg'=>"Plan");
        //     $this->data = $ok_data;
        //  }else{
        //     $error_data = array('logger'=>False,'msg'=>'Error Plan');
        //     $this->data = $error_data;                
        //  } 
         
         $ok_data = array('logger'=>True,'msg'=>"Plan");
         $this->data = $ok_data;           
      }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $obj = Load::model($this->model);   
        $json = $this->param(); 
        $costeoOperativo = $json['costeoOperativo'];
        $obj = $obj->find_first((int) $id);
        $obj->plan_id = $costeoOperativo['plan_id'];
        $obj->detalle = $costeoOperativo['detalle'];
        $obj->cantidad = $costeoOperativo['cantidad'];
        $obj->valor = $costeoOperativo['valor'];

        // delete plan Item Costeo
        $plan_item_costeo_balance_delete = load::model('plan_costeo_balance')->find("conditions: plan_id=$id");
        foreach ($plan_item_costeo_balance_delete as $itemCosteoBalance) {
            $itemCosteoBalance->delete("plan_id=$id");
        }

        // Guardo Item Costeo
            foreach ($costeoOperativo as $itemCosteoNew) {
                $plan_item_costeo_balance = new PlanCosteoBalance();
                $plan_item_costeo_balance->plan_id = $id;
                $plan_item_costeo_balance->detalle = $itemCosteoNew['detalle'];
                $plan_item_costeo_balance->cantidad = $itemCosteoNew['cantidad'];
                $plan_item_costeo_balance->valor = $itemCosteoNew['valor'];
                $plan_item_costeo_balance->save();
    
            }


        $ok_data = array('logger'=>True,'msg'=>"Plan",$costeoOperativo);
        $this->data = $ok_data;
       
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        // $obj = Load::model($this->model);
        // $obj = $obj->find_first((int) $id);
        // $obj->estado = -1;                
        // if ($obj->update($this->param())) {
        //     $this->setCode(200);
        //     $this->data = $obj;
        // } else {
        //     $error_data = array('logger'=>False,'msg'=>'Error');
        //     $this->data = $error_data;
        // }

    }

}
