<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("cotizacion",'cotizacion_historial');  // carga modelos
Load::lib('bitly'); // cargo lib

class CotizacionCronjobController extends RestController {

    public $model = 'cotizacion';

    /**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {    
        $this->data = array('logger'=>True,'msg'=>'');
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
      $cotizaciones = load::model('cotizacion')->find("conditions: estado =1");
      $count_item = 0;
      foreach ($cotizaciones as $itemCotizacion) {
        $fecha_entero_hoy = strtotime(date("Y-m-d",time()));
        $itemCotizacion->fecha_entero_hoy = $fecha_entero_hoy;
        $fechaSumaDia = date("Y-m-d",strtotime($itemCotizacion->cotizacion_at."+ $itemCotizacion->vencimiento days")); 
        $itemCotizacion->fecha_suma = $fechaSumaDia;
        $fecha_entero =strtotime($fechaSumaDia);
        $itemCotizacion->fecha_entero = $fecha_entero;

        if($fecha_entero_hoy >=$fecha_entero){
          $itemCotizacion->estado = 0;
          $count_item = $count_item+1;
          if($itemCotizacion->update()){
            $cotizacionHistorial = new CotizacionHistorial();
            $cotizacionHistorial->cotizacion_id = $itemCotizacion->id;
            $cotizacionHistorial->detalle = "Cotización cerrada por vencimiento de tiempo.";
            $cotizacionHistorial->usuario_id =  $itemCotizacion->usuario_id;
            $cotizacionHistorial->estado = 1;
            $cotizacionHistorial->save();
          }
          $itemCotizacion->count_item = $count_item;
        }
      }
      $count_cotizaciones = load::model('cotizacion')->count("estado=1");

      $this->data = array('logger'=>True,'msg'=>"",'count'=>$count_cotizaciones,'count_item'=>$count_item);
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

    public function post() {
        $this->data = array('logger'=>True,'msg'=>'Post');                
    }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {
        $this->data = array('logger'=>True,'msg'=>'');         
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $this->data = array('logger'=>True,'msg'=>'');
    }

}
