<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("cliente",'cotizacion','cotizacion_item','cotizacion_item_precio','cotizacion_item_costeo');  // carga modelos
Load::lib('bitly'); // cargo lib

class CotizacionesController extends RestController {

    public $model = 'cliente';

    /**
     * Retorna un registro a través de su $id 
     * metodo get objeto/:id
     */
    public function get($id) {    
        $cotizacion = load::model("cotizacion")->find_first("conditions: id=$id");
        $cotizaciones_item = load::model('cotizacion_item')->find("conditions: cotizacion_id=$id");
            foreach($cotizaciones_item as $item){
                $producto = load::model('producto')->find_first("conditions: id=$item->producto_id");
                $item->icono = $producto->icono;

                $cotizacion_item_precio = load::model('cotizacion_item_precio')->find("conditions: cotizacion_id=$id");
                $item->cotizacion_item_precio = $cotizacion_item_precio;
            }

        $this->data = array('logger'=>True,'msg'=>"OK",'cotizacion'=>$cotizacion,'cotizaciones_item'=>$cotizaciones_item,'cotizacion_item_precio'=>$cotizacion_item_precio);        
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $cotizaciones = Load::model('cotizacion')->find("order: id desc");
        foreach ($cotizaciones as $cotizacion) {
          $cliente = load::model('cliente')->find_first("conditions: id=$cotizacion->cliente_id");
          $cotizacion->cliente = $cliente->nombre;
          
          $items_cliente = Load::model('cotizacion_item')->count("cotizacion_id =$cotizacion->id AND estado >=1");          
          $cotizacion->items = $items_cliente;
        }
        $this->data = array('logger'=>True,'msg'=>"OK",'cotizacion'=>$cotizaciones);        

    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

    public function post() {
        $json = $this->param(); 
        $cliente_id = $json['cliente_id'];
        $estado = $json['estado'];
        // $cotizacion_item = $json['cotizacion_item'];
        $campos = !empty($json['cotizacion_item']) ? $json['cotizacion_item'] : '';
        $camposItemPrecios = !empty($json['cotizacion_item_precio']) ? $json['cotizacion_item_precio'] : '';
        // $cotizacion_precio = $json['cotizacion_precio'];
        $cotizacion_id = $json['cotizacion_id'];

        $usuario_id = Session::get('id');



        $cotizacion = new Cotizacion();
        $cotizacion->cliente_id = $cliente_id;
        $cliente = load::model('cliente')->find_first("conditions: id=$cliente_id");
        $cotizacion->estado = $estado;
        $cotizacion->usuario_id = $usuario_id;

        // VALIDO HAY ID DE COTIZACION PARA CREAR NUEVA O SOLO AGREGAR ITEM
        if($cotizacion_id == NULL){

            if($cotizacion->save()){

                // CONSULTO TRM
                $trm = Load::model('trm')->find_first("order: id desc");

                // PRECIO COTIZACION
                if(!empty($camposItemPrecios)){
                    foreach($camposItemPrecios as $CItemPrecio){
                        $cotizacion_item_precio = New CotizacionItemPrecio();
                        $cotizacion_item_precio->cotizacion_id = $cotizacion->id;
                        $cotizacion_item_precio->detalle = $CItemPrecio['detalle'];
                        $cotizacion_item_precio->cantidad = !empty($CItemPrecio['cantidad']) ? $json['cantidad'] : '0';
                        $cotizacion_item_precio->trm = $trm->valor;
                        $cotizacion_item_precio->valor_unidad = !empty($CItemPrecio['cantidad']) ? $CItemPrecio['valor_unidad'] : '0';
                        $cotizacion_item_precio->valor_usd = !empty($CItemPrecio['cantidad']) ? $CItemPrecio['valor_usd'] : '0';
                        $cotizacion_item_precio->valor = $CItemPrecio['valor'];
                        $cotizacion_item_precio->save();
                    }
                }

    
                // ITEM COTIZACION
                $cotizacion_item = New CotizacionItem();
                $cotizacion_item->cotizacion_id = $cotizacion->id;
                $cotizacion_item->estado = $estado;
                $cotizacion_item->producto =  $campos['producto'];
                $cotizacion_item->origen = $campos['origen'];
                $cotizacion_item->destino = $campos['destino'];
                $cotizacion_item->fechain = $campos['fechain'];
                $cotizacion_item->fecha_out = $campos['fecha_out'];
                $cotizacion_item->adultos = $campos['adultos'];
                $cotizacion_item->infates = $campos['infantes'];
                $cotizacion_item->habitaciones = $campos['habitaciones'];
                $cotizacion_item->descripcion = $campos['descripcion'];
                $cotizacion_item->mascara_tarifa = $campos['mascara_tarifa'];
                $cotizacion_item->titulo = $campos['titulo'];
                $cotizacion_item->valor = $campos['valor'];
                $cotizacion_item->utilidad = $campos['utilidad'];
                $cotizacion_item->valor_total = $campos['valor_total'];
                $cotizacion_item->noches = $campos['noches'];
                $cotizacion_item->producto_id = $campos['producto_id'];
    
    
    
                $key_token = Security::setKey($cotizacion->id, 'shw_cliente');
                $cotizacion->token = $key_token;
    
                // ACORTADOR DE URL
                $dominio  = DOMINIO;
                $params = array();
                $params['access_token'] = 'a6bbdaa7dc961c56962810ea00c9b33e6bb90ab1';
                $params['longUrl'] = $dominio.'reporte/cotizacion/ver/'.$cotizacion->token;
                // $params['longUrl'] = "$dominio"."reporte/cotizacion/ver/".$cotizacion->token;
                $params['domain'] = 'j.mp';
                $results = bitly_get('shorten', $params);
                $results_data = $results['data'];
                $cotizacion->url_corta = $results_data['url'];
                $cotizacion->update();
    
                $cotizacion_item->cotizacion_id = $cotizacion->id;
                $cotizacion_item->estado = 1;        
                if($cotizacion_item->save()){
                    // $this->setCode(200);
                    $ok_data = array('logger'=>True,'msg'=>"cotizacion item ","cotizacion_id" => $cotizacion->id,'url_corta'=>"$cotizacion->url_corta",'celular'=>"$cliente->celular",'token'=>"$cotizacion->token");
                    $this->data = $ok_data;
        
                }else{
                    // $this->setCode(400);
                    $error_data = array('logger'=>False,'msg'=>"Error Cotizacion item");
                    $this->data = $error_data;                    
                }
    
            }else{
                // $this->setCode(400);
                $error_data = array('logger'=>False,'msg'=>'Error Cotizacion');
                $this->data = $error_data;                
            }


        // FIN VALIDO HAY ID DE COTIZACION PARA CREAR NUEVA O SOLO AGREGAR ITEM
        }else{

                // PRECIO COTIZACION
                $cotizacion_item_precio = New CotizacionItemPrecio();
    
                // ITEM COTIZACION
                $cotizacion_item = New CotizacionItem();
                $cotizacion_item->cotizacion_id = $cotizacion_id;
                $cotizacion_item->estado = $estado;
                $cotizacion_item->producto =  $campos['producto'];
                $cotizacion_item->origen = $campos['origen'];
                $cotizacion_item->destino = $campos['destino'];
                $cotizacion_item->fechain = $campos['fechain'];
                $cotizacion_item->fecha_out = $campos['fecha_out'];
                $cotizacion_item->adultos = $campos['adultos'];
                $cotizacion_item->infates = $campos['infantes'];
                $cotizacion_item->habitaciones = $campos['habitaciones'];
                $cotizacion_item->descripcion = $campos['descripcion'];
                $cotizacion_item->mascara_tarifa = $campos['mascara_tarifa'];
                $cotizacion_item->titulo = $campos['titulo'];
                $cotizacion_item->valor = $campos['valor'];
                $cotizacion_item->utilidad = $campos['utilidad'];
                $cotizacion_item->valor_total = $campos['valor_total'];
                $cotizacion_item->noches = $campos['noches'];
                $cotizacion_item->producto_id = $campos['producto_id']; 
                $cotizacion_item->estado = 1;        
                if($cotizacion_item->save()){
                    // $this->setCode(200);
                    $ok_data = array('logger'=>True,'msg'=>"cotizacion item ","cotizacion_id" => $cotizacion_id);
                    $this->data = $ok_data;
        
                }else{
                    // $this->setCode(400);
                    $error_data = array('logger'=>False,'msg'=>"Error Cotizacion item");
                    $this->data = $error_data;                    
                } 
           
        
        } // FIN VALIDACION
         
    }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $json = $this->param(); 
        $cotizacion_item = $json['cotizacion_item'];
        $cotizacion_item_precios = $json['cotizacion_item_precio'];

        $obj = Load::model('cotizacion_item');
        $obj = $obj->find_first((int) $id);
        $obj->producto = $cotizacion_item['producto'];
        $obj->origen = $cotizacion_item['origen'];
        $obj->destino = $cotizacion_item['destino'];
        $obj->fechain = $cotizacion_item['fechain'];
        $obj->fecha_out = $cotizacion_item['fecha_out'];
        $obj->adultos = $cotizacion_item['adultos'];
        $obj->infantes = $cotizacion_item['infantes'];
        $obj->habitaciones = $cotizacion_item['habitaciones'];
        $obj->descripcion = $cotizacion_item['descripcion'];
        $obj->mascara_tarifa = $cotizacion_item['mascara_tarifa'];
        $obj->noches = $cotizacion_item['noches'];
        $obj->titulo = $cotizacion_item['titulo'];

        if ($obj->update($this->param())) {


            // BORRO PRECIOS COTIZACION
            $cotizacion_item_precio_delete = load::model('cotizacion_item_precio')->find("conditions: cotizacion_id =$obj->cotizacion_id");
            foreach ($cotizacion_item_precio_delete as $cotizacion_precio_delete) {
                // Flash::valid("$cotizacion_delete->id - $cotizacion_delete->cotizacion_id");
                $cotizacion_precio_delete->delete("cotizacion_id=$obj->cotizacion_id");
            }

            // CONSULTO TRM
            $trm = Load::model('trm')->find_first("order: id desc");
            // PRECIO COTIZACION
            foreach($cotizacion_item_precios as $CItemPrecio){
                $cotizacion_item_precio = New CotizacionItemPrecio();
                $cotizacion_item_precio->cotizacion_id = $obj->cotizacion_id;
                $cotizacion_item_precio->detalle = $CItemPrecio['detalle'];
                $cotizacion_item_precio->cantidad = $CItemPrecio['cantidad'];
                $cotizacion_item_precio->valor_unidad = $CItemPrecio['valor_unidad'];
                $cotizacion_item_precio->trm = $trm->valor;
                $cotizacion_item_precio->valor_usd = $CItemPrecio['valor_usd'];
                $cotizacion_item_precio->valor = $CItemPrecio['valor'];
                $cotizacion_item_precio->save();
            }


            $this->setCode(202);
            $ok_data = array('logger'=>True,'msg'=>'Actualizado Exitosamente','descripcion'=>$cotizacion_item['descripcion']);                
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }
         
    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model($this->model);
        $obj = $obj->find_first((int) $id);
        $obj->estado = -1;                
        if ($obj->update($this->param())) {
            $this->setCode(200);
            $this->data = $obj;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
