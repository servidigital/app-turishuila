<?php

/**
 * Controlador base para la construcción de API REST para modelos rápidamente
 *
 * @category Kumbia
 * @package Controller
 */

Load::models("vehiculo","vehiculo_servicio","vehiculo_plantilla");  // carga modelos

class VehiculosController extends RestController {

    public $model = 'vehiculo';

/**
     * Retorna un registro a través de su $id
     * metodo get objeto/:id
     */
    public function get($id) {
        $this->data = Load::model($this->model)->find((int) $id);
    }

    /**
     * Lista los registros
     * metodo get objeto/
     */
    public function getAll() {
        $data = Load::model($this->model)->find("order: nombre asc","conditions: estado=1");
        foreach ($data as $item_data) {
          $vehiculo_servicios = load::model('vehiculo_servicio')->find("conditions: vehiculo_id=$item_data->id");
          $item_data->vehiculo_serivicios = $vehiculo_servicios;

          $vehiculo_plantilla = load::model('vehiculo_plantilla')->find("conditions: vehiculo_id=$item_data->id");
          $item_data->vehiculo_plantilla = $vehiculo_plantilla;          
        }

        $this->data = $data;
    }

    /**
     * Crea un nuevo registro
     * metodo post objeto/
     */

      public function post() {
        $json = $this->param();
        $campos = $json['vehiculo'];
        // $campos2 = $json['servicio'];
        $camposSillas = $json['sillaRow'];
        $estado = $json['estado'];

            $crear = new Vehiculo();
            $crear->nombre = $campos['nombre'];
            $crear->placa = $campos['placa'];
            $crear->sillas = $campos['sillas'];
            $crear->estado = $estado;

            if($crear->save()){
                // foreach($campos2 as $item){
                //     $crear_item = new VehiculoServicio();
                //     $crear_item->vehiculo_id = $crear->id;
                //     $crear_item->nombre = $item['nombre'];
                //     $crear_item->tipo_servicio = $item['tipo_servicio'];
                //     if($crear_item->save()){
                //         // $ok_data = array('logger'=>True,'msg'=>'Creado Crear2');
                //         // $this->data = $ok_data;
                //     }else{
                //         // $error_data = array('logger'=>False,'msg'=>'Error Crear2 ');
                //         // $this->data = $error_data;
                //     }
                // }

                foreach($camposSillas as $itemSilla){
                    $crear_item = new VehiculoPlantilla();
                    $crear_item->vehiculo_id = $crear->id;
                    $crear_item->tipo = $itemSilla['tipo'];
                    $crear_item->silla = $itemSilla['silla'];
                    if($crear_item->save()){
                        // $ok_data = array('logger'=>True,'msg'=>'Creado Sillas');
                        // $this->data = $ok_data;
                    }else{
                        // $error_data = array('logger'=>False,'msg'=>'Error Sillas ');
                        // $this->data = $error_data;
                    }
                }                

                $this->data = array('logger'=>True,'campo'=>$campos);

            }else{
                $error_data = array('logger'=>False,'msg'=>'Error Crear ');
                $this->data = $error_data;
            }


    }



    /**
     * Modifica un registro por $id
     * metodo put objeto/:id
     */
    public function put($id) {

        $json = $this->param();
        $vehiculoID = $json['vehiculo_id'];
        $campo = $json['vehiculo'];
        $campo2 = $json['sillaRow'];

        $obj = new Vehiculo();
        $obj = $obj->find_first((int) $id);
        $obj->nombre = $campo['nombre'];
        $obj->placa = $campo['placa'];
        $obj->sillas = $campo['sillas'];
        if ($obj->update()){

            // Borro la plantilla
            $vehiculo_plantillas = load::model('vehiculo_plantilla')->find("conditions: vehiculo_id=$obj->id");
            foreach ($vehiculo_plantillas as $itemPlantilla) {
                // $plantilla = new VehiculoPlantilla(); 
                $itemPlantilla->delete($itemPlantilla->id);
            }

            // creo la nueva plantilla 
            foreach ($campo2 as $item) {
                $vPlantilla = new VehiculoPlantilla();
                $vPlantilla->vehiculo_id = $vehiculoID;
                $vPlantilla->tipo = $item['tipo'];
                $vPlantilla->silla = $item['silla'];
                $vPlantilla->save();
            }

            $this->setCode(202);
            $ok_data = array('logger'=>True,'msg'=>'Actualizado Exitosamente');
            $this->data = $ok_data;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }       

    }

    /**
     * Elimina un registro por $id
     * metodo delete objeto/:id
     */
    public function delete($id) {
        $obj = Load::model($this->model);
        $obj = $obj->find_first((int) $id);
        $obj->estado = -1;
        if ($obj->update($this->param())) {
            $this->setCode(200);
            $this->data = $obj;
        } else {
            $error_data = array('logger'=>False,'msg'=>'Error');
            $this->data = $error_data;
        }

    }

}
