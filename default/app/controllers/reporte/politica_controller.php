<?php
/**
 *
 * Descripcion: Controlador
 *
 * @category
 * @package     Controllers
 */

load::models('politica');

class PoliticaController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'politicas';
    }

    public function pdf($key) {
        $id = Security::getKey($key, 'shw_cliente', 'int');
        if(empty($id)) {
            return View::error();
        }
        View::template(NULL);
        $politica = New Politica();
        $politica = $politica->find_first($id);
        $this->$politica = $politica;
    }

    public function ver($key,$formato='html') {
        $id = Security::getKey($key, 'shw_cliente', 'int');
        if(empty($id)) {
            return View::error();
        }

        View::template(NULL);
        $politica = New Politica();
        $politica = $politica->find_first("conditions: id='$id' ");

        $this->politica = $politica;

        $this->page_format = 'html';
        $this->page_title = 'Politicas';

    }
}
