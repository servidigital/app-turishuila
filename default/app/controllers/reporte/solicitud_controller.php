<?php
/**
 *
 * Descripcion: Controlador
 *
 * @category
 * @package     Controllers
 */

load::models('cotizacion','cotizacion_item','solicitud');

class SolicitudController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'reportes';
    }

    public function pdf($key) {
        $id = Security::getKey($key, 'shw_cliente', 'int');
        if(empty($id)) {
            return View::error();
        }
        View::template(NULL);
        $cotizacion = new Cotizacion();
        $cotizacion = $cotizacion->find_first($id);

        $this->$cotizacion = $cotizacion;
//        $this->page_format = $formato;

    }

    public function ver($token,$formato='html') {

        View::template(NULL);
        $cotizacion = New Cotizacion();
        $cotizacion = $cotizacion->find_first("conditions: token='$token' ");
        $cotizacion->visita_cliente = $cotizacion->visita_cliente +1;
        $cotizacion->save();


        $this->cotizacion = $cotizacion;

        $cotizacion_items = New CotizacionItem();
        $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$cotizacion->id");

        $this->page_format = 'html';
        $this->page_title = 'Reporte Html';

    }


    public function crear(){
        view::template(NULL);
        $this->estadoForm = "";

        if(Input::hasPost('nombre') && Input::hasPost('email') && Input::hasPost('celular') && Input::hasPost('detalle') ) {
            $nombre = Input::post("nombre");
            $email = Input::post("email");
            $celular = Input::post("celular");
            $ciudad = Input::post("ciudad");
            $detalle = Input::post("detalle");

            $cliente = New Cliente();
            
            if($find_cliente = $cliente->find_first("conditions: email ='$email'")){
                $solicitud = New Solicitud();
                $solicitud->cliente_id = $find_cliente->id;
                $solicitud->detalle = $detalle;
                $solicitud->color = "#ff00ff";
                $solicitud->usuario_id = 1;
                if($solicitud->save()){
                    Flash::valid('La Solicitud se ha creado correctamente.');
                    $this->estadoForm = "1";
                }else{
                    Flash::error('Error al registrar solicitud');
                }
            }else{

                $cliente->nombre = $nombre;
                $cliente->email = $email;
                $cliente->celular = $celular;
                $cliente->ciudad = $ciudad;
                $cliente->estado = 1;
                $cliente->nota = "Cliente Registrado desde Web";
                if($cliente->save()){
                    $solicitud = New Solicitud();
                    $solicitud->cliente_id = $cliente->id;
                    $solicitud->detalle = $detalle;
                    $solicitud->color = "#FF00FF";
                    $solicitud->usuario_id = 1;    
                    if($solicitud->save()){
                        Flash::valid('La Solicitud se ha creado correctamente.');
                        $this->estadoForm = "1";
                    }
                }else{
                    Flash::error('Error al registrar solicitud');
                }
            }


        }
    }
}