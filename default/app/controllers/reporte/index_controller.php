<?php
/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category    
 * @package     Controllers 
 */

load::models('cotizacion','cotizacion_item','cliente');

class IndexController extends BackendController {
        
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'reportes';
    }
    
    public function index($key) {
        
    }
    
    public function reporte($key,$formato='html') {
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');            
        }
        View::template(NULL);        
        $cotizacion = New Cotizacion();
        $this->cotizacion = $cotizacion->find_first($id);
        
        $cotizacion_items = New CotizacionItem(); 
        $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$id AND estado=1");
        
        $this->page_format = 'html';
        $this->page_title = 'Reporte Html';  
        
    } 
    
    public function clientes($formato='xls') {
        View::select('clientes', NULL);

        $this->informe_clientes = Load::model('cliente')->find("conditions: estado=1 ","order: id DESC");

        $this->page_format = 'xls';
        $this->page_title = "Informe - Clientes";

    }    
    
}
