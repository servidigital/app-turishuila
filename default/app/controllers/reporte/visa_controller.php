<?php
/**
 *
 * Descripcion: Controlador
 *
 * @category
 * @package     Controllers
 */

load::models('visa','visa_formato','visa_adjunto');

class VisaController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'reportes';
    }

    public function pdf($key) {
        $id = Security::getKey($key, 'shw_cliente', 'int');
        if(empty($id)) {
            return View::error();
        }
        View::template(NULL);
        $cotizacion = new Cotizacion();
        $cotizacion = $cotizacion->find_first($id);

        $this->$cotizacion = $cotizacion;
//        $this->page_format = $formato;

    }

    public function ver($token,$formato='html') {

        View::template(NULL);
        $visa = New Visa();
        $visa = $visa->find_first("conditions: token='$token' ");
        $visa->visita_cliente = $visa->visita_cliente +1;
        $visa->save();


        $this->visa = $visa;

        $visa_formato = New VisaFormato();
        $this->visa_formato = $visa_formato->find_first("id=$visa->visa_formato_id");

        $visa_adjunto = New VisaAdjunto();
        $this->visa_adjunto = $visa_adjunto->find("conditions: visa_id=$visa->id");

        $this->page_format = 'html';
        $this->page_title = 'formato visa cliente';

    }

    public function formato($id,$token) {

        View::template(NULL);
        $visa_formato = New VisaFormato();
        $this->visa_formato = $visa_formato->find_first("id=$id");

        $this->page_format = 'html';
        $this->page_title = 'formato visa';

    }    
}
