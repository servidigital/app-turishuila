<?php
/**
 * Dailyscript - Web | App | Media
 *
 * Descripcion: Controlador que se encarga de la visualización de los reportes de las acciones en el sistema
 *
 * @category
 * @package     Controllers
 * @author      Iván D. Meléndez (ivan.melendez@dailycript.com.co)
 * @copyright   Copyright (c) 2013 Dailyscript Team (http://www.dailyscript.com.co)
 */

Load::models('cursos/certificado');

class CertificadoController extends BackendController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        $this->page_title = 'Información del certificado';
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Certificados';
    }

    /**
     * Método para ver la información de un certificado
     * @param type $key
     * @return type
     */
    public function ver($key='', $formato='html') {
        $id = DwSecurity::isValidKey($key, 'imp_certificado', 'int');
        if(empty($id)) {
            return View::error();
        }
        $certificado = new Certificado();
        $certificado = $certificado->getInformacionCertificado($id);
        if(empty($certificado->printer)) {
            if($certificado->estado != Certificado::PENDIENTE) {
                $upd = new Certificado();
                $upd->find_first($id);
                $upd->printer = 1;
                $upd->update();
                $certificado = $certificado->getInformacionCertificado($id);
            }
        }
        $this->certificado = $certificado;
        $this->page_format = $formato;
    }

    /**
     * Metodo para imprimir el reporte
     */
    public function filtro($corte, $empresa, $tipo, $estado, $order='order.fecha_inicio.asc', $formato='xls') {
        $empresa    = Filter::get($empresa, 'page');
        $certificado = new Certificado();
        $this->certificados = $certificado->getFiltroCertificado($corte, $empresa, $tipo, $estado, $order);

        $t = new TipoCurso();

        $tmp = explode( '|', $tipo);
        if(count($tmp) == 1) {
            $tipo = $t->getInformacionTipoCurso(Filter::get($tipo,'page'));
        } else {
            $tipo = $t->getTipoCursoToString($tipo);
        }

        $this->corte = $corte;
        $this->tipo = $tipo;
        $this->estado = $estado;
        $this->order = $order;
        $this->page_format = 'xls';
        $this->page_title = 'Listado de certificados por filtro';
    }

    /**
     * Metodo para imprimir el reporte
     */
    public function listar($estado, $order='order.fecha_inicio.asc', $formato='xls') {
        $empresa            = Filter::get($empresa, 'page');
        $certificado        = new Certificado();
        $this->certificados = $certificado->getListadoCertificado('find', $estado, $order);
        $this->estado       = $estado;
        $this->order        = $order;
        $this->page_format  = 'xls';
        $this->page_title   = 'Listado de certificados por estado';
    }

}

