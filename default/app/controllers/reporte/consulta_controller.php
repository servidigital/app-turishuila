<?php	 		 	;
/**
 * Dailyscript - Web | App | Media
 *
 * Descripcion: Controlador que se encarga de la visualización de los reportes de las acciones en el sistema
 *
 * @category
 * @package     Controllers
 * @author      Iván D. Meléndez (ivan.melendez@dailycript.com.co)
 * @copyright   Copyright (c) 2013 Dailyscript Team (http://www.dailyscript.com.co)
 */

Load::models('cursos/certificado');
Load::lib('fpdf17/fpdf');

class ConsultaController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        $this->page_title = 'Información del certificado';
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Certificados';

        $this->set_title = FALSE;

    }

    /**
     * Método para ver la información de un certificado
     * @param type $key
     * @return type
     */
    public function certificado($key='', $formato='html') {
        $id = DwSecurity::isValidKey($key, 'imp_certificado_frontend', 'int');
        if(empty($id)) {
            return View::error();
        }

        $certificado = new Certificado();
        $this->certificado = $certificado->getInformacionCertificado($id);
        View::report($formato);
    }

}

