<?php
// Require composer autoload
require_once APP_PATH . '../../vendor/autoload.php';
use Mpdf\Mpdf;
// Load::lib('mpdf/mpdf');


/**
 *
 * Descripcion: Controlador para el panel principal de los usuarios logueados
 *
 * @category
 * @package     Controllers
 */

load::models('voucher','cotizacion');

class VouchersController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'reportes';
    }

    public function index($key) {

    }

    public function reporte($key,$formato='html') {
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }

    //Importante: Sin vista y sin tamplate
    // View::select(null, null);
    //Crea una instancia de la clase y le pasa el directorio default/app/temp/
    // $mpdf = new Mpdf(['tempDir' => APP_PATH . '/temp']);
    //Escribe algo de contenido HTML:
    // $mpdf->WriteHTML('¡Hola KumbiaPHP!');
    //Envía un archivo PDF directamente al navegador
    // $mpdf->Output();



        // View::template(NULL);
        // $cotizacion = New Cotizacion();
        // $cotizacion = $cotizacion->find_first($id);

        $vouchers_items = New Voucher();
        $vouchers = $vouchers_items->find("cotizacion_id=$id");
        $this->vouchers = $vouchers_items->find("cotizacion_id=$id");

        $this->page_format = 'html';
        $page_title = 'Reporte Html';
        $this->page_title = 'VOUCHER '.$id;

        // // Activa el almacenamiento en búfer de la salida
        // ob_start();
        // // Carga el contenido del partial pasandole datos
        // View::partial('backend/pdf/voucher_pdf', '', ['vouchers' => $vouchers, 'page_title' => $page_title ]);
        // // Obtiene en $html el contenido del búfer actual y elimina el búfer de salida actual
        // $html = ob_get_clean();
        // // Crea una instancia de la clase y le pasa el directorio temporal
        // $mpdf = new Mpdf(['tempDir' => APP_PATH . '/temp']);
        // // Escribe algo de contenido HTML:
        // // $mpdf->Image(DOMINIO.'img/logo-turishuila.png', 0, 0, 210, 297, 'png', '', true, false);

        // // $stylesheet = file_get_contents(DOMINIO.'css/bootstrap/bootstrap.min.css');
        // // $stylesheet = file_get_contents(DOMINIO.'css/backend/style.css');
        // // $mpdf->WriteHTML($stylesheet);
        // $mpdf->autoPageBreak = true;

        // // $mpdf->SetWatermarkText('TURISHUILA');
        // // $mpdf->showWatermarkText = true;
        // $mpdf->SetWatermarkImage('./img/marca-agua-logo.jpg',0.1,50,array(40,100));
        // $mpdf->showWatermarkImage = true;

        // $mpdf->WriteHTML($html);

        // // Obliga la descarga del PDF y se personaliza el nombre
        // $mpdf->Output();
    }

    public function reportepdf($key,$formato='pdf') {
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');
        }
        View::select('reportepdf.pdf', NULL);

        $cotizacion = New Cotizacion();
        $this->cotizacion = $cotizacion->find_first($id);

        $vouchers_items = New Voucher();
        $this->vouchers = $vouchers_items->find("cotizacion_id=$id");

        $this->page_format = 'pdf';
        $this->page_title = "Reporte $fecha";

    }

}
