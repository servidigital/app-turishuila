<?php
/**
 *
 * Descripcion: Controlador
 *
 * @category
 * @package     Controllers
 */

load::models('cotizacion','cotizacion_item');

class CotizacionController extends AppController {

    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'reportes';
    }

    public function pdf($key) {
        $id = Security::getKey($key, 'shw_cliente', 'int');
        if(empty($id)) {
            return View::error();
        }
        View::template(NULL);
        $cotizacion = new Cotizacion();
        $cotizacion = $cotizacion->find_first($id);

        $this->$cotizacion = $cotizacion;
//        $this->page_format = $formato;

    }

    public function ver($token,$formato='html') {

        View::template(NULL);
        $cotizacion = New Cotizacion();
        $cotizacion = $cotizacion->find_first("conditions: token='$token' ");
        $cotizacion->visita_cliente = $cotizacion->visita_cliente +1;
        $cotizacion->save();


        $this->cotizacion = $cotizacion;

        $cotizacion_items = New CotizacionItem();
        $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$cotizacion->id");

        $this->page_format = 'html';
        $this->page_title = 'Cotización Turishuila Click en el enlace';

    }
}
