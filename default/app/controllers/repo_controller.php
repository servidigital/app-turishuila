<?php

/**
 * Controller para los reportes publicos
 * 
 */

load::models('cotizacion','cotizacion_item');

class RepoController extends AppController {

    public $page_title = "Reporte";
    
    public function index() {

    }
    
    public function ver($key) {
        if(!$id = Security::getKey($key, 'shw_cliente', 'int')) {
            return Redirect::toAction('/');            
        }
        View::template(NULL);
        $cotizacion = New Cotizacion();
        $cotizacion = $cotizacion->find_first($id);
        $cotizacion->visita_cliente = $cotizacion->visita_cliente +1;
        $cotizacion->save();
        
        
        $this->cotizacion = $cotizacion;
        
        $cotizacion_items = New CotizacionItem(); 
        $this->cotizacion_items = $cotizacion_items->find("cotizacion_id=$id");
        
        $this->page_format = 'html';
        $this->page_title = 'Reporte Html';        

    }    

}
