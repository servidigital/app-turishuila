<?php
/**
 *
 * Descripcion: Clase que gestiona las politicas
 *
 * @category
 * @package     Models politica
 * @subpackage 
 */

class Politica extends ActiveRecord {
     
   public function GetListadopolitica($order='', $page=0) {
        $columns = 'politica.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'politica.estado AS', 
                'DESC'=>'politica.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }


    /**
     * Método para buscar politica
     */
    public function searchPolitica($value) {
        $columns = 'politica.*';
        $conditions = " politica.nombre LIKE '%$value%'";
        return $this->find("columns: $columns", "conditions: $conditions");
    }        
    
    /**
     * Método para buscar politica
     */
    public function getAjaxPoliticas($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'politica.*';
        
        
        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }                
        
        $conditions = " $field LIKE '%$value%'";
        
        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }  
    }      
     
  
    
}?>