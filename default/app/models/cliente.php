<?php
/**
 *
 * Descripcion: Clase que gestiona los Clientes
 *
 * @category
 * @package     Models cliente
 * @subpackage 
 */

class cliente extends ActiveRecord {
     
   public function GetListadoCliente($order='', $page=0) {
        $columns = 'cliente.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'cliente.estado AS', 
                'DESC'=>'cliente.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }


    /**
     * Método para buscar clientes para contrato
     */
    public function searchCliente($value) {
        $columns = 'cliente.*';
        $conditions = " cliente.nombre LIKE '%$value%'";
        return $this->find("columns: $columns", "conditions: $conditions");
    }        
    
    /**
     * Método para buscar Clientes
     */
    public function getAjaxClientes($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cliente.*';
        
        
        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }                
        
        $conditions = " $field LIKE '%$value%'";
        
        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }  
    }      
     
  
    
}?>