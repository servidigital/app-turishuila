<?php
/**
 *
 * Descripcion: Clase que gestiona los productos
 *
 * @category
 * @package     Models Producto 
 * @subpackage 
 */

class Producto extends ActiveRecord {
    
   public function GetListadoproducto($order='', $page=0) {
        $columns = 'producto.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'producto.estado AS', 
                'DESC'=>'producto.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }


    /**
     * Método para buscar producto
     */
    public function searchProveedor($value) {
        $columns = 'producto.*';
        $conditions = " producto.nombre LIKE '%$value%'";
        return $this->find("columns: $columns", "conditions: $conditions");
    }        
    
    /**
     * Método para buscar producto
     */
    public function getAjaxProveedores($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cliente.*';
        
        
        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }                
        
        $conditions = " $field LIKE '%$value%'";
        
        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }  
    }      
          
  
    }      
       
?>