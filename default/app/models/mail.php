<?php 

    //Carga las librería PHPMailer
    Load::lib('phpmailer/class.smtp');
    Load::lib('phpmailer/class.phpmailer');

    class Mail {

        public static function send($para_correo, $para_nombre, $asunto, $cuerpo, $de_correo=null, $de_nombre=null) {

            //instancia de PHPMailer
            $mail = new PHPMailer(true);
            $mail->CharSet = "UTF-8";
            $mail->IsSMTP();
            $mail->SMTPDebug = 1;
            $mail->SMTPAuth = true; // enable SMTP authentication
            $mail->SMTPSecure = 'ssl'; // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";
            $mail->Port = "465";
            $mail->Username = "sistemas@turishuila.com";
            $mail->Password = '8707Carlosandres';

            if ($de_correo != null && $de_nombre != null) {
                $mail->AddReplyTo($de_correo, $de_nombre);
                $mail->From = $de_correo;
                $mail->FromName = $de_nombre;
            } else {
                $mail->AddReplyTo("diego.vargas@turishuila.com", "Diego Vargas");
                $mail->From = "no-reply@turishuila.com";
                $mail->FromName = "Turishuila APP";
            }

            $mail->Subject = $asunto;
            $mail->Body = $cuerpo;
            $mail->WordWrap = 50; // set word wrap
            $mail->MsgHTML($cuerpo);
            $mail->AddAddress($para_correo, $para_nombre);
            $mail->IsHTML(true); // send as HTML
            $mail->SetLanguage('es');
            //Enviamos el correo
            $exito = $mail->Send();
            $intentos = 2;
            //esto se realizara siempre y cuando la variable $exito contenga como valor false
            while ((!$exito) && $intentos < 1) {
                sleep(5);
                $exito = $mail->Send();
                $intentos = $intentos + 1;
            }

            $mail->SmtpClose();
            return $exito;
        }
    
}