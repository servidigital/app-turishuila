<?php
/**
 *
 * Descripcion: Clase que gestiona las cotizacion
 *
 * @category
 * @package     Models cliente
 * @subpackage
 */
Load::models('cliente');

class Cotizacion extends ActiveRecord {

   public function GetListadoCotizaciones($order='', $page=0, $usuario_id='') {
        // $usuario_id = Session::get('id');
        $columns = 'cotizacion.*';

        $order = $this->get_order($order, array(
            'estado' => array(
                'ASC'=>'cotizacion.estado AS',
                'DESC'=>'cotizacion.estado DESC'
            )
        ));

        if( (Session::get('perfil_id') == Perfil::SUPER_USUARIO) || (Session::get('perfil_id') == Perfil::ADMINISTRADOR) || (Session::get('perfil_id') == Perfil::SUPERVISOR)  || (Session::get('perfil_id') == Perfil::COMERCIAL)) {
            if(!empty($usuario_id)){
                if($page) {
                    return $this->paginated("columns: $columns","conditions: usuario_id=$usuario_id","order: $order", "page: $page");
                } else {
                    return $this->find("columns: $columns","conditions: usuario_id=$usuario_id", "order: $order");
                }
            }else{
                if($page) {
                    return $this->paginated("columns: $columns","order: $order", "page: $page");
                } else {
                    return $this->find("columns: $columns", "order: $order");
                }
            }


        }  else {
            if($page) {
                return $this->paginated("columns: $columns","conditions: usuario_id=$usuario_id" ,"order: $order", "page: $page");
            } else {
                return $this->find("columns: $columns","conditions: usuario_id=$usuario_id", "order: $order");
            }
        }


    }


    /**
     * Método para buscar Clientes
     */
    public function getAjaxClientes($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cliente.*';


        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }

        $conditions = " $field LIKE '%$value%'";

        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }
    }



    /**
     * Método para buscar Solicitudes del Cliente
     */
    public function getAjaxSolicitudesClientes($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cotizacion.*,cliente.nombre';
        $join= 'INNER JOIN cliente ON cliente.id = cotizacion.cliente_id ';

        //Defino los campos habilitados para la búsqueda
        $fields = array('nombre','numero');
        if(!in_array($field, $fields)) {
            $field = 'cliente.nombre';
            $conditions = " $field LIKE '%$value%'";
        }else{
            $field = 'cotizacion.id';
            $conditions = " $field LIKE '$value'";
        }

        if( (Session::get('perfil_id') == Perfil::SUPER_USUARIO) || (Session::get('perfil_id') == Perfil::ADMINISTRADOR) || (Session::get('perfil_id') == Perfil::SUPERVISOR)) {
            if($page) {
                return $this->paginated("columns: $columns","join: $join", "conditions: $conditions", "page: $page");
            } else {
                return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
            }
        }
    }

    public function getAjaxCotizacionesClientes($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cotizacion.*,cliente.nombre';
        $join= 'INNER JOIN cliente ON cliente.id = cotizacion.cliente_id ';

        //Defino los campos habilitados para la búsqueda
        $fields = array('nombre','numero');
        if(!in_array($field, $fields)) {
            $field = 'cliente.nombre';
        }else{
            $field = 'cotizacion.id';
        }
        $conditions = " $field LIKE '%$value%'";

        // if( (Session::get('perfil_id') == Perfil::SUPER_USUARIO) || (Session::get('perfil_id') == Perfil::ADMINISTRADOR) || (Session::get('perfil_id') == Perfil::SUPERVISOR)) {
            if($page) {
                return $this->paginated("columns: $columns","join: $join", "conditions: $conditions", "page: $page");
            } else {
                return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
            }
        // }
    }

}?>