<?php
/**
 *
 * Descripcion: Clase que gestiona los Proveedor
 *
 * @category
 * @package     Models proveedor
 * @subpackage 
 */

class Proveedor extends ActiveRecord {
     
   public function GetListadoproveedor($order='', $page=0) {
        $columns = 'proveedor.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'proveedor.estado AS', 
                'DESC'=>'proveedor.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }


    /**
     * Método para buscar proveedor
     */
    public function searchProveedor($value) {
        $columns = 'proveedor.*';
        $conditions = " proveedor.nombre LIKE '%$value%'";
        return $this->find("columns: $columns", "conditions: $conditions");
    }        
    
    /**
     * Método para buscar proveedor
     */
    public function getAjaxProveedores($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'cliente.*';
        
        
        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }                
        
        $conditions = " $field LIKE '%$value%'";
        
        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }  
    }      
     
  
    
}?>