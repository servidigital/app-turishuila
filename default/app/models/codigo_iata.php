<?php
/**
 *
 * Descripcion: Clase que gestiona los IATA
 *
 * @category
 * @package     Models politica
 * @subpackage 
 */

class CodigoIata extends ActiveRecord {
    
   public function GetListadoiata($order='', $page=0) {
        $columns = 'codigo_iata.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'codigo_iata.estado AS', 
                'DESC'=>'codigo_iata.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }    
    
    
    /**
     * Método para obtener las ciudades como json
     * @return type
     */
    public function getIatasToJson() {
        $rs = $this->find("columns: codigo, ciudad", 'order: ciudad ASC');
        $iatas = array();
        foreach($rs as $iata) {
            $iatas[] = $iata->ciudad.' - '.$iata->codigo;
        }
        return json_encode($iatas);
    }      
    
}?>