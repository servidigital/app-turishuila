<?php
/**
 *
 * Descripcion: Clase que gestiona las politicas
 *
 * @category
 * @package     Models solicitud
 * @subpackage
 */

class Solicitud extends ActiveRecord {

   public function GetListadosolicitud($order='', $page=0) {
        $columns = 'solicitud.*';

        $order = $this->get_order($order, array(
            'estado' => array(
                'DESC'=>'solicitud.estado DESC',
                'ASC'=>'solicitud.estado AS'
            )
        ));


        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }
    }



}?>