<?php
/**
 *
 * Descripcion: Clase que gestiona los Proveedor
 *
 * @category
 * @package     Models proveedor
 * @subpackage 
 */

class TarjetaAsistencia extends ActiveRecord {
     
   public function GetListadoTarjetas($order='', $page=0) {
        $columns = 'tarjeta_asistencia.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'tarjeta_asistencia.estado AS', 
                'DESC'=>'tarjeta_asistencia.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }


    /**
     * Método para buscar tarjeta
     */
    public function searchTarjeta($value) {
        $columns = 'tarjeta_asistencia.*';
        $conditions = " tarjeta_asistencia.nombre LIKE '%$value%'";
        return $this->find("columns: $columns", "conditions: $conditions");
    }        
    
    /**
     * Método para buscar proveedor
     */
    public function getAjaxTarjetas($field, $value, $order='', $page=0) {
        $value = Filter::get($value, 'string');
        if( strlen($value) <= 2 OR ($value=='none') ) {
            return NULL;
        }
        $columns = 'tarjeta_asistencia.*';
        
        
        //Defino los campos habilitados para la búsqueda
        $fields = array('email', 'nombre', 'cc');
        if(!in_array($field, $fields)) {
            $field = 'nombre';
        }                
        
        $conditions = " $field LIKE '%$value%'";
        
        if($page) {
            return $this->paginated("columns: $columns", "conditions: $conditions", "page: $page");
        } else {
            return $this->find("columns: $columns", "join: $join", "conditions: $conditions");
        }  
    }      
     
  
    
}?>