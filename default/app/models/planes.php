<?php
/**
 *
 * Descripcion: Clase que gestiona las planes
 *
 * @category
 * @package     Models planes
 * @subpackage 
 */

class Planes extends ActiveRecord {
         
    public function GetListadoplanes($order='', $page=0) {
        $columns = 'planes.*';
                
        $order = $this->get_order($order, array(                        
            'estado' => array(
                'ASC'=>'planes.estado AS', 
                'DESC'=>'planes.estado DESC'
            )
        ));
        

        if($page) {
            return $this->paginated("columns: $columns", "order: $order", "page: $page");
        } else {
            return $this->find("columns: $columns", "order: $order");
        }  
    }    
    
}?>