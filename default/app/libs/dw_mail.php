<?php
/**
 * Dailyscript - app | web | media
 *
 *
 *
 * @category    Librería para el manejo de e-mail
 * @package     Libs
 * @author      Iván D. Meléndez
 * @copyright   Copyright (c) 2011 Dailyscript Team (http://www.dailyscript.com.co)
 * @version     1.0
 */

/**
 * Cargador para la libreria PHPMailer
 *
 **/
Load::lib('phpmailer/class.smtp');
Load::lib('phpmailer/class.phpmailer');

class DwMail extends PHPMailer {

    /**
     * CharSet del mensaje
     * @var string
     */
    public $CharSet = APP_CHARSET;

    /**
     * Tipo de contenido
     * @var string
     */
    public $ContentType = 'text/html';

    /**
     * Defino quien envía el mail
     * @var string
     */
    public $From = 'servidigitalneiva@gmail.com';

    /**
     * Nombre de quien lo envía
     * @var string
     */
    public $FromName = "APP_Turishuila";

    /**
     * Nombre del host
     * @var type
     */
    public $Host = 'smtp.gmail.com';

    /**
     * Nombre del usuario SMTP
     * @var string
     */
    public $Username = 'servidigitalneiva@gmail.com';

    /**
     * Clave del usuario SMTP
     * @var string
     */
    public $Password = '8707Carlosandres';

    /**
     * Método constructor
     */
    public function __construct($exceptions = false) {
        $this->SetLanguage('es', APP_PATH . 'libs/phpmailer/language/');
        $this->PluginDir = APP_PATH . 'libs/phpmailer/';
        $this->SMTPDebug = (PRODUCTION) ? 0 : 1;
        parent::__construct($exceptions);
    }

    /**
     * Método para signar el motivo del email
     * @param string $text
     */
    public function Subject($text) {
        $this->Subject = $text;
    }

    /**
     * Método para crear el cuerpo
     * @param string
     */
    public function BodyHtml() {
//        ob_start();
//        View::partial("mail/$partial", false, $params);
        $this->Body;
    }

    /**
     * Método para definir el SMTP
     * @param type $port
     * @param type $secure
     */
    public function SMTPConfig($port, $secure='') {
        $this->Port = $port;
        if(!empty($secure)) {
            $this->SMTPSecure = $secure;
        }
        $this->SMTPAuth = TRUE;
        parent::IsSMTP();
    }

    public function SendMail() {
        $exito = $this->Send();
        if(!$exito) {
            $exito = $this->Send();
        }
        return $exito;
    }


}
?>



