<?php
/**
 * @see Controller nuevo controller
 */
require_once CORE_PATH . 'kumbia/controller.php';
require_once CORE_PATH . 'extensions/helpers/html.php';

/**
 * Controlador principal que heredan los controladores
 *
 * Todas las controladores heredan de esta clase en un nivel superior
 * por lo tanto los metodos aqui definidos estan disponibles para
 * cualquier controlador.
 *
 * @category Kumbia
 * @package Controller
 */

require_once APP_PATH . 'extensions/helpers/dw_form.php';
require_once APP_PATH . 'extensions/helpers/dw_html.php';
require_once APP_PATH . 'extensions/helpers/dw_js.php';

Load::models('sistema/usuario');

Load::lib('dw_config');

DwConfig::load();

class AppController extends Controller {

    final protected function initialize() {

    }

    final protected function finalize() {

    }

}
