<?php

/**
 * Transactions
 *
 * @category KumbiaPHP
 * @package View
 */

// Cargo cualquier modelo (es indiferente para las transacciones)
Load::models('sistema/usuario');

class Transaction {

    /**
     * Instancia del active record
     * @var type
     */
    private static $_obj;

    /**
     * Variable que indica si está corriendo o no una transacción
     */
    public static $running = FALSE;


    /**
     * Inicia transacción para cualquier evento
     */
    public static function start($cb=NULL) {

        // Si no hay instancia del active record cree una
        if(!self::$_obj) {

            // Se requiere un objecto de active record para poder
            // invocar las transacciones en la bd
            self::$_obj = new Usuario();

        }

        if(!self::$running) {

            self::$running  = TRUE;
            self::$_obj->begin();

        }

        if ($cb instanceof Closure) {
            return Transaction::end( $cb() );
        }

    }

    /**
     * Confirma transacción para cualquier evento
     */
    public static function commit() {

        if(self::$running) {

            self::$running  = FALSE;
            self::$_obj->commit();
        }

    }

    /**
     * Cancela transacción para cualquier evento
     */
    public static function rollback() {

        if(self::$running) {

            self::$running  = FALSE;
            self::$_obj->rollback();
        }

    }

    /**
     * Finaliza una transacción conforme al resultado
     */
    public static function end($result) {

        ($result) ? self::commit() : self::rollback();

        return $result;

    }

}
