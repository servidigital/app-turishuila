<?php	 		 	;
/**
 * Dailyscript - app | web | media
 *
 * Tamaño 286 para 'l' con 5 de x
 * Tamaño 186 para 'p'
 *
 * @category    Librería para el manejo de pdf's
 * @package     Libs
 * @author      Iván D. Meléndez
 * @copyright   Copyright (c) 2011 Dailyscript Team (http://www.dailyscript.com.co)
 * @version     1.0
 */

Load::lib('fpdf/fpdf');

class DwPDF extends FPDF {

    public $B=0;
    public $I=0;
    public $U=0;
    public $HREF='';
    public $ALIGN='';

    public $mc_x;
    public $mc_y;
    public $current_x;
    public $current_y;
    public $mc_h = 4;
    public $mc_min = 4;
    public $mc_break = 0;

    /**
     * Indica si muestra el título en la cabecera
     * @var type
     */
    public $show_header = FALSE;
    /**
     * Texto en la cabecera del reporte
     * @var type
     */
//    public $text_header = APP_CLIENT;
    /**
     * Indica si muestra el texto en el pie
     * @var type
     */
    public $show_footer = FALSE;
    /**
     * Texto a mostrar en el pie de página
     * @var type
     */
    public $text_footer = APP_NAME;
    /**
     * Indica si el PDF es un documento o reporte general
     * @var type
     */
    public $is_document = FALSE;

    /**
     * Variable que contiene la información de la empersa
     */
    protected $_empresa;

    /**
     * Método constructor
     * @param type $orientation
     * @param type $unit
     * @param type $format
     */
    public function  __construct($orientation='P', $unit='mm', $format='A4') {
        parent::FPDF($orientation, $unit, $format);
        $this->SetAuthor(Filter::get(APP_NAME.' - '.date("Y"), 'utf8'));
        $this->_empresa = Load::model('config/empresa')->getInformacionEmpresa();
    }

    /**
     * Monta el header
     */
    function Header() {
        $empresa = $this->_empresa;
        if($this->is_document == TRUE) {
            $this->Image(dirname(APP_PATH)."/public/img/upload/empresa/$empresa->logo",20,8,150);
            $this->Ln(5);
        } else if($this->show_header) {
            $this->SetFont('Arial','',9);
            $this->SetY(10);
            $this->Cell(0,10,Filter::get(APP_NAME.' - '.$this->text_header, 'utf8'),0,0,'L');
            $this->Ln(10);
        }
    }

    function Footer() {
        $empresa = $this->_empresa;
        if($this->is_document == TRUE) {
            $this->Cell(0,10,html_entity_decode('P&aacute;gina ').$this->PageNo().' de {nb}',0,0);
        } else if($this->show_footer) {
            $this->SetFont('Arial','',8);
            $this->SetY(-12);
            $this->Cell(0,10,html_entity_decode('P&aacute;gina ').$this->PageNo().' de {nb}'.' - '.date("Y-m-d H:i:s"),0,0);
        }
    }

    function WriteHTML($html) {
        //HTML parser
        $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e) {
            if($i%2==0) {
                //Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                    $this->Cell(0,5,$e,0,1,'C');
                else
                    $this->Write(5,$e);
            }
            else {
                //Tag
                if($e[0]=='/')
                    $this->CloseTag(strtoupper(substr($e,1)));
                else {
                    //Extract properties
                    $a2=explode(' ',$e);
                    $tag=strtoupper(array_shift($a2));
                    $prop=array();

                    foreach($a2 as $v) {
                        if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                            $prop[strtoupper($a3[1])]=$a3[2];
                    }
                    $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop) {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF=$prop['HREF'];
        if($tag=='BR')
                $this->Ln(5);
        if($tag=='P')
            $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR') {
            if( !empty($prop['WIDTH']) )
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag) {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF='';
        if($tag=='P')
            $this->ALIGN='';
    }

    function SetStyle($tag,$enable) {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
            if($this->$s>0)
                $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt) {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }


    /**
     * Método para crear una página
     * @param type $titulo
     * @param type $subtitulo
     */
    function newPage($titulo='',$subtitulo='') {
        $this->AliasNbPages();
        $this->AddPage();
        if($titulo != '') {
            if($this->is_document == true) {
                $this->SetY(35);
            }
            $this->SetFont('Arial','B',11);
            $this->Cell(0,5,html_entity_decode($titulo),0,0,'C');
            if($subtitulo != '') {
                $this->Ln();
                $this->Cell(0,5,html_entity_decode($subtitulo),0,0,'C');
                $this->SetY(32);
            } else {
                $this->SetY(24);
            }
            $this->Ln(10);
        }
    }

    /**
     * Métdo para mostrar un subtítulo
     * @param type $subtitulo
     * @param type $break
     * @param type $align
     */
    function setSubtitle($subtitulo, $break=true, $align='C') {
        if($break) {
            $this->ln();
        }
        $this->SetFont('Arial','B',9);
        $this->Cell(0,5,html_entity_decode(strtoupper($subtitulo)),0,0,  strtoupper($align));
        $this->ln();
    }


    /**
     * Método para setear las cabeceras del datagrid
     */
    function setFormatDatagridHeader($margin=true) {
        if($margin) {
            $this->SetTopMargin(10);
            $this->SetLeftMargin(10);
            $this->SetRightMargin(10);
        }
        $this->SetFont('Arial','B',8);
        $this->SetFillColor(200,200,200);
        $this->SetTextColor(0);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.2);

    }


    /**
     * Método para setear las filas del datagrid
     */
    public function setFormatDatagridRow() {
        $this->SetFont('Arial','',8);
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(0);
    }

    /**
     * Métdo para imprimr una línea en un datagrid
     * @param type $width
     * @param type $text
     * @param type $align
     * @param type $height
     * @param type $border
     */
    public function setCellDatagrid($width, $text, $align='C', $height=6, $border='TRBL') {
        $this->Cell($width, $height, $text, $border, 0, $align);
    }


    /**
     * Método para mostrar una imagen de la carpeta upload
     * @param string $file
     * @param type $x
     * @param type $y
     * @param type $w
     * @param type $h
     * @param type $message
     * @return boolean
     */
    public function getImagen($file, $x=5, $y=null, $w=198, $h=0, $message='') {
        $file = dirname(APP_PATH)."/public/img/upload/".CUSTOM_FOLDER."/$file";
        if(is_file($file)) {
            $this->Image($file, $x, $y, $w, $h);
            return true;
        } else {
            if($message) {
                $this->SetFont('Arial', '', 9);
                $this->SetY($y);
                $this->SetX($x);
                $this->Cell(0, 5, Filter::get($message, 'utf8'));
                $this->Ln();
            }
            return false;
        }
    }


    /**
     * Método para descargar el pdf
     * @param type $title
     * @param type $x
     * @param type $tam
     * @param type $text
     */
    public function getOutput($title,$x='',$tam='',$text='') {
        if($x !='') {
            $this->SetX($x);
            $this->SetFont('Arial','B',8);
            $this->Cell($tam,7,Filter::get($text, 'utf8'),'LRB',0,'L');
            $this->Ln();
        }
        $title = trim($title, '.');
        $this->Output(Filter::get($title, 'utf8').".pdf",'I');
    }

    /**
     *
     * @param type $w
     * @param type $txt
     * @param int $border
     * @param string $align
     * @param boolean $fill
     */
    public function MC($w, $txt, $border=0, $align='J', $fill=false) {
        $this->MultiCell($w, $this->mc_h, Filter::get($txt, 'utf8'), $border, $align, $fill);
        $after_y = $this->GetY();
        if($after_y > $this->mc_y) {
            $this->mc_y = $after_y;
        }
        $this->current_x = $this->current_x + $w;
        $this->SetXY($this->current_x, $this->current_y);
    }

    /**
     *
     */
    public function StartMC($h=4, $min=1) {
        $this->mc_x = $this->getX();
        $this->mc_y = $this->getY();
        $this->current_x = $this->GetX();
        $this->current_y = $this->GetY();
        $this->mc_h = $h;
        $this->mc_min = ($min > 0) ? $min : 1;
        //echo "Actual: $this->current_y - ";
    }

    /**
     *
     */
    public function EndMC() {
        $tmp = $this->mc_h * $this->mc_min;
        $dif = ($this->mc_y - $this->current_y) + ($this->mc_break * $this->mc_h);
        if($tmp > $dif) {
            $this->mc_y = $this->mc_y + ($tmp - $dif);
        }
        $this->SetXY($this->mc_x, $this->mc_y);
    }

    /**
     *
     */
    public function BreakMC() {
        $this->SetXY($this->mc_x, $this->current_y);
        $this->Ln();
        $this->current_x = $this->GetX();
        $this->current_y = $this->GetY();
        $this->mc_break++;
    }


}
?>
