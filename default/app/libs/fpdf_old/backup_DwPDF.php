<?php	 		 	;
/**
 * Dailyscript - app | web | media
 *
 * Tamaño 286 para 'l' con 5 de x
 * Tamaño 186 para 'p'
 *
 * @category    Librería para el manejo de pdf's
 * @package     Libs
 * @author      Iván D. Meléndez
 * @copyright   Copyright (c) 2011 Dailyscript Team (http://www.dailyscript.com.co)
 * @version     1.0
 */

Load::lib('fpdf17/fpdf');

class BackupDwPDF extends FPDF {

    public $B=0;
    public $I=0;
    public $U=0;
    public $HREF='';
    public $ALIGN='';

    public function  __construct($orientation='P', $unit='mm', $format='A4') {
        parent::FPDF($orientation, $unit, $format);
    }

    function Header() {
        //Pon tu código aquí
    }

    function Footer() {
        //Pon tu código aquí
    }

    function WriteHTML($html) {
        //HTML parser
        $html=str_replace("n",' ',$html);
        $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);

        foreach($a as $i=>$e) {
            if($i%2==0) {
                //Text
                if($this->HREF)
                        $this->PutLink($this->HREF,$e);
                elseif($this->ALIGN=='center')
                        $this->Cell(0,5,$e,0,1,'C');
                else
                        $this->Write(5,$e);
            }
            else {
                //Tag
                if($e[0]=='/')
                        $this->CloseTag(strtoupper(substr($e,1)));
                else {
                        //Extract properties
                        $a2=explode(' ',$e);
                        $tag=strtoupper(array_shift($a2));
                        $prop=array();

                        foreach($a2 as $v) {

                          if(preg_match('/([^=]*)=[""]?([^""]*)/',$v,$a3))
                                        $prop[strtoupper($a3[1])]=$a3[2];
                        }
                        $this->OpenTag($tag,$prop);
                }
            }
        }
    }

    function OpenTag($tag,$prop) {
        //Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
                $this->SetStyle($tag,true);
        if($tag=='A')
                $this->HREF=$prop['HREF'];
        if($tag=='BR')
                $this->Ln(5);
        if($tag=='P')
                $this->ALIGN=$prop['ALIGN'];
        if($tag=='HR') {
            if( !empty($prop['WIDTH']) )
                    $Width = $prop['WIDTH'];
            else
                    $Width = $this->w - $this->lMargin-$this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x,$y,$x+$Width,$y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag) {
        //Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
                $this->SetStyle($tag,false);
        if($tag=='A')
                $this->HREF='';
        if($tag=='P')
                $this->ALIGN='';
    }

    function SetStyle($tag,$enable) {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach(array('B','I','U') as $s)
                if($this->$s>0)
                        $style.=$s;
        $this->SetFont('',$style);
    }

    function PutLink($URL,$txt) {
        //Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
    }

    function setFormatDatagridHeader($margin=true) {
        if($margin) {
            $this->SetTopMargin(10);
            $this->SetLeftMargin(10);
            $this->SetRightMargin(10);
        }
        $this->SetFont('Arial','B',8);
        $this->SetFillColor(200,200,200);
        $this->SetTextColor(0);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.2);

    }

    public function getOutput($title,$x='',$tam='',$text='') {
        if($x !='') {
            $this->SetX($x);
            $this->SetFont('Arial','B',8);
            $this->Cell($tam,7,Filter::get($text, 'utf8'),'LRB',0,'L');
            $this->Ln();
        }
        $title = trim($title, '.');
        $this->Output(Filter::get($title, 'utf8').".pdf",'I');
    }

    public function setFormatDatagridRow() {
        $this->SetFont('Arial','',8);
        $this->SetFillColor(255,255,255);
        $this->SetTextColor(0);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(0);
    }

    public function uploadImagen($file, $x=5, $y=null, $w=198, $h=0, $message='') {
        $file = dirname(APP_PATH)."/public/img/upload/$file";
        if(is_file($file)) {
            $this->Image($file, $x, $y, $w, $h);
            return true;
        } else {
            if($message) {
                $this->SetFont('Arial', '', 9);
                $this->SetY($y);
                $this->SetX($x);
                $this->Cell(0, 5, Filter::get($message, 'utf8'));
                $this->Ln();
            }
            return false;
        }
    }

    public function setCellDatagrid($width, $text, $align='C', $height=6, $border='TRBL') {
        $this->Cell($width, $height, $text, $border, 0, $align);
    }

}
?>
