<?php
/**
 *
 * Descripcion: Controlador para las Preguntas
 *
 * @category    
 * @package     Controllers 
 */
Load::models('examen','examen_pregunta','examen_respuesta');  // carga modelos

class PreguntasController extends BackendController {
    
    /**
     * Método que se ejecuta antes de cualquier acción
     */
    protected function before_filter() {
        //Se cambia el nombre del módulo actual
        $this->page_module = 'Gestión de Preguntas';
    }
    

    /**
     * Método para agregar
     */
    public function agregar($key) {
        if(!$id = Security::getKey($key, 'upd', 'int')) {
            return Redirect::toAction('listar');
        }        
        if(Input::hasPost('pregunta')) {
            ActiveRecord::beginTrans();
            $pregunta = new ExamenPregunta(Input::post('pregunta'));
            $pregunta->estado = 1;
            $pregunta->examen_id = $id;
            if($pregunta->save()) {
                ActiveRecord::commitTrans();
                Flash::valid('La Pregunta se ha creado correctamente.');
                return Redirect::to("dashboard/examenes/editar/$key/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->page_title = 'Agregar Pregunta'; 
    }


    /**
     * Método para ver
     */
	public function ver($key) {        
	        if(!$id = Security::getKey($key, 'shw', 'int')) {
	            return Redirect::toAction('listar');
	        }
	        
	        $curso = new Curso();
	        if(!$curso->find_first($id)) {
	            Flash::error('Lo sentimos, no se ha podido establecer la información del Pregunta');    
	            return Redirect::toAction('listar');
	        }                	                
	        $this->curso = $curso;
	    
	} 
        
        
    /**
     * Método para editar
     */    
    public function editar($key,$curso_id) {        
        if(!$id = Security::getKey($key, 'upd', 'int')) {
            return Redirect::toAction('listar');
        }
        $this->key  = $key;
        $this->curso_id = $curso_id;
        $pregunta = new ExamenPregunta();
        if(!$pregunta->find_first($id)) {
            Flash::error('Lo sentimos, no se ha podido establecer la información de la Pregunta');    
            return Redirect::toAction('listar');
        }                
        
        if(Input::hasPost('pregunta')) {            
            ActiveRecord::beginTrans();            
            $pregunta = new ExamenPregunta(Input::post('pregunta'));
            $pregunta->find_first($id);
            if($pregunta->update(Input::post('pregunta'))){
                ActiveRecord::commitTrans();
                $key_shw = Security::setKey($curso_id, 'upd');
                Flash::valid("La Pregunta se ha actualizado correctamente.");
                return Redirect::to("dashboard/examenes/editar/$key_shw/");
            } else {
                ActiveRecord::rollbackTrans();
            } 
        }     
        
        if(Input::hasPost("pregunta_id","respuesta","estado")){
//            var_dump($_POST['examen_respuesta']);
            $pregunta_id = Input::post("pregunta_id");
            $pre_respuesta = Input::post("respuesta");
            $estado = Input::post("estado");
            $img_respuesta = Input::post("respuesta_img");
            if($estado == NULL){ $estado = "0";}  else { $estado ="1";}

//                Flash::info("Estado : $pre_respuesta Pregutan: $pregunta_id Estado: $estado");
//                return Redirect::to("dashboard/preguntas/editar/$key/$curso_id/");

            
            ActiveRecord::beginTrans();            
            $respuesta = new ExamenRespuesta();
            $respuesta->find_first("$pregunta_id");
            $respuesta->respuesta = $pre_respuesta;
            $respuesta->estado = $estado;
            $respuesta->img = $img_respuesta;
            if($respuesta->update()){
                ActiveRecord::commitTrans();
//                $key_shw = Security::setKey($curso_id, 'upd');
                Flash::valid("Respuesta Actualizada Exitosamente");
                return Redirect::to("dashboard/preguntas/editar/$key/$curso_id/");
            } else {
                ActiveRecord::rollbackTrans();
            }            
        }
        $this->pregunta = $pregunta;
        $this->page_title = 'Actualizar Pregunta';
        
        
    }        
    /**
     * Método para Eliminar Respuesta
     */        
    public function eliminar($key,$curso_id,$respuesta_id){
        if(!$id = Security::getKey($key, 'upd', 'int')) {
            return Redirect::toAction('listar');
        }
        $respuesta = New ExamenRespuesta();
        $respuesta->find_first("$respuesta_id");
        if ($respuesta->delete((int)$respuesta_id)) {
                Flash::valid('Respuesta Eliminada exitosamente');
                return Redirect::to("dashboard/preguntas/editar/$key/$curso_id/");                
        }else{
                Flash::error('Falló Operación'); 
        }        
        
    }
    /**
     * Método para Inactivar Curso
     */    
    public function bloquear($key,$key_examen_id) {        
        if(!$id = Security::getKey($key, 'blo', 'int')) {
            return Redirect::toAction('listar');
        }
        $pregunta = new ExamenPregunta();
        $pregunta->find_first("$id");
        if($pregunta->estado ==1){
            $pregunta->estado = "0";
            $estado= "Pregunta Inactivo";            
        }  else {
            $pregunta->estado = "1";     
            $estado= "Pregunta Activo";            
        }
        if ($pregunta->update()){
            Flash::info("$estado");
                return Redirect::to("dashboard/examenes/editar/$key_examen_id");
        }
    }      
  
    
    /**
     * Método para subir imágenes
     */
    public function upload() {     
        $upload = new DwUpload('img', 'img/upload/respuestas/');
        $upload->setAllowedTypes('png|jpg|gif|jpeg');
        $upload->setEncryptName(TRUE);
        $upload->setSize('3MB', 400, 400, TRUE);
        if(!$data = $upload->save()) { //retorna un array('path'=>'ruta', 'name'=>'nombre.ext');
            $data = array('error'=>TRUE, 'message'=>$upload->getError());
        }
        sleep(1);//Por la velocidad del script no permite que se actualize el archivo
        $this->data = $data;
        View::select(NULL, 'json');
    }      

}
